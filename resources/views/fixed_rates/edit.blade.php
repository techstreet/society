@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Fixed Rate</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/fixed-rates')}}">Fixed Rates</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('fixed-rates.update', $FixedRate->id) }}" method="post">
                        {{ method_field('put') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="capacity_charge">*Load Charge</label>
                            <input type="number" class="form-control" name="capacity_charge" value="{{$FixedRate->capacity_charge}}" min="0" step="0.01" placeholder="Enter load charge" required>
                        </div>
                        <div class="form-group">
                            <label for="unit_charge">*Unit Charge</label>
                            <input type="number" class="form-control" name="unit_charge" value="{{$FixedRate->unit_charge}}" min="0" step="0.01" placeholder="Enter unit charge" required>
                        </div>
                        <div class="form-group">
                            <label for="applicable_from">*Applicable From</label>
                            <input type="text" class="form-control" id="datePicker" name="applicable_from" value="{{date_dfy($FixedRate->applicable_from)}}" placeholder="Enter applicable from" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection