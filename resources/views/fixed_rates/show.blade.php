@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Fixed Rate</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/fixed-rates')}}">Fixed Rates</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Load Charge</label>
                            <p>{{$FixedRate->capacity_charge}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Unit Charge</label>
                            <p>{{$FixedRate->unit_charge}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Applicable From</label>
                            <p>{{date_dfy($FixedRate->applicable_from)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection