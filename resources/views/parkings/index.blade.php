@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Parkings</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">Parkings</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-heading mb-15 clearfix">
                    <a class="btn btn-info btn-addon btn-sm pull-right" href="{{url('/parkings/create')}}">
                        <i class="fa fa-plus"></i>Add New</a>
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tower</th>
                                <th>Floor</th>
                                <th>Flat</th>
                                <th>Parking Level</th>
                                <th>Parking Zone</th>
                                <th>Parking Number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($parkings as $key=>$value)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td>{{$value->tower_name}}</td>
                                <td>{{$value->floor_name}}</td>
                                <td>{{$value->flat_name}}</td>
                                <td>{{$value->parking_level}}</td>
                                <td>{{$value->parking_zone}}</td>
                                <td>{{$value->parking_number}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{url('/parkings/'.$value->id)}}" class="btn btn-primary">
                                            <i class="fa fa-eye"></i> View </a>
                                        <a href="{{url('/parkings/'.$value->id.'/edit')}}" class="btn btn-success">
                                            <i class="fa fa-pencil"></i> Edit </a>
                                        <form class="form-inline" action="{{ url('/parkings', ['id' => $value->id]) }}" method="post">
                                            {{ method_field('delete') }} {{ csrf_field() }}
                                            <button class="btn btn-danger" onclick="return confirm('Are you sure you want to Delete?');">
                                                <i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection