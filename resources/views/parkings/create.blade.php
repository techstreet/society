@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Add Parking</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/parkings')}}">Parkings</a>
            </li>
            <li class="active">Add</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('parkings.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">*Tower</label>
                                    <select class="form-control" name="tower_id" id="towerOptions" onChange="getFloors(this.value);" required>
                                        <option value="">Select</option>
                                        @foreach($towers as $value)
                                        <option value="{{$value->tower_id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Floor</label>
                                    <select class="form-control" name="floor_id" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group">
                            <label for="parking_level">*Parking Level</label>
                            <label class="radio-inline">
                                <input type="radio" name="parking_level" value="Ground" checked>Ground
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="parking_level" value="B1">B1
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="parking_level" value="B2">B2
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="parking_zone">*Parking Zone</label>
                            <input type="text" class="form-control" name="parking_zone" placeholder="Enter parking zone" required>
                        </div>
                        <div class="form-group">
                            <label for="parking_number">*Parking Number</label>
                            <input type="text" class="form-control" name="parking_number" placeholder="Enter parking number" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
</script> @endsection