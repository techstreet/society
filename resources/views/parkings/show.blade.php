@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Parking</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/parkings')}}">Parkings</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Tower</label>
                            <p>{{$parking->tower_name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Floor</label>
                            <p>{{$parking->floor_name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Flat</label>
                            <p>{{$parking->flat_name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Parking Level</label>
                            <p>{{$parking->parking_level}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Parking Zone</label>
                            <p>{{$parking->parking_zone}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Parking Number</label>
                            <p>{{$parking->parking_number}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection