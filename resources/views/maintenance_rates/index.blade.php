@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Maintenance Rates</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">Maintenance Rates</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-heading mb-15 clearfix">
                    <a class="btn btn-info btn-addon btn-sm pull-right" href="{{url('/maintenance-rates/create')}}">
                        <i class="fa fa-plus"></i>Add New</a>
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Flat Type</th>
                                <th>Type</th>
                                <th>Rate</th>
                                <th>Applicable From</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($maintenance_rates as $key=>$value)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td>{{$value->flat_type_name}}</td>
                                <td>
                                    @if($value->type == '1') Fixed @endif
                                    @if($value->type == '2') Per Square Feet @endif
                                </td>
                                <td>{{$value->rate}}</td>
                                <td>{{date_dfy($value->applicable_from)}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{url('/maintenance-rates/'.$value->id)}}" class="btn btn-primary">
                                            <i class="fa fa-eye"></i> View </a>
                                        <a href="{{url('/maintenance-rates/'.$value->id.'/edit')}}" class="btn btn-success">
                                            <i class="fa fa-pencil"></i> Edit </a>
                                        <form class="form-inline" action="{{ url('/maintenance-rates', ['id' => $value->id]) }}" method="post">
                                            {{ method_field('delete') }} {{ csrf_field() }}
                                            <button class="btn btn-danger" onclick="return confirm('Are you sure you want to Delete?');">
                                                <i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection