@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Maintenance Rate</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/maintenance-rates')}}">Maintenance Rates</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('maintenance-rates.update', $MaintenanceRate->id) }}" method="post">
                        {{ method_field('put') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="flat_type_id">*Flat Type</label>
                            <select class="form-control" name="flat_type_id" required>
                                @foreach($flat_types as $value)
                                <option value="{{$value->id}}" @if($MaintenanceRate->flat_type_id == $value->id) selected @endif>{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="type">*Type</label>
                            <br/>
                            <label>
                                <input type="radio" name="type" value="1" @if($MaintenanceRate->type == '1') checked @endif>Fixed</label>
                            <label>
                                <input type="radio" name="type" value="2" @if($MaintenanceRate->type == '2') checked @endif>Per Square Feet</label>
                        </div>
                        <div class="form-group">
                            <label for="rate">*Rate</label>
                            <input type="number" class="form-control" name="rate" value="{{$MaintenanceRate->rate}}" min="0" step="0.01" placeholder="Enter rate" required>
                        </div>
                        <div class="form-group">
                            <label for="applicable_from">*Applicable From</label>
                            <input type="text" class="form-control" id="datePicker" name="applicable_from" value="{{date_format(date_create($MaintenanceRate->applicable_from),'d-M-Y')}}" min="0" placeholder="Enter applicable from" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection