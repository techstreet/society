@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Maintenance Rate</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/maintenance-rates')}}">Maintenance Rates</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Flat Type</label>
                            <p>{{$MaintenanceRate->flat_type_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Type</label>
                            <p>
                                @if($MaintenanceRate->type == '1') Fixed @endif @if($MaintenanceRate->type == '2') Per Square Feet @endif
                            </p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Rate</label>
                            <p>{{noneIfEmpty($MaintenanceRate->rate)}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Applicable From</label>
                            <p>{{date_dfy($MaintenanceRate->applicable_from)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection