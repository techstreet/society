<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Title -->
    <title>Admin Panel</title>

    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta charset="UTF-8">
    <meta name="description" content="Admin Panel" />
    <meta name="theme-color" content="#f5f5f5">

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/plugins/pace-master/themes/blue/pace-theme-flash.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/uniform/css/uniform.default.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/fontawesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/line-icons/simple-line-icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/waves/waves.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/3d-bold-navigation/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/slidepushmenus/css/component.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" />

    <!-- Theme Styles -->
    <link href="{{asset('assets/css/modern.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/themes/'.themeColor().'.css')}}" class="theme-color" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css" />

    <!-- Data Table -->
    <link href="{{asset('assets/plugins/datatables/css/jquery.datatables.css')}}" rel="stylesheet" type="text/css" />

    <!-- Jquery -->
    <script src="{{asset('assets/plugins/jquery/jquery-2.1.4.min.js')}}"></script>

    <!-- Show/Hide password -->
    <script src="{{asset('assets/js/hideShowPassword.min.js')}}" type="text/javascript"></script>

    <!-- Others -->
    <script src="{{asset('assets/plugins/3d-bold-navigation/js/modernizr.js')}}"></script>
    <script src="{{asset('assets/plugins/offcanvasmenueffects/js/snap.svg-min.js')}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body class="page-header-fixed page-sidebar-fixed">
    <div class="overlay"></div>
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s1">
        <h3>
            <span class="pull-left">Chat</span>
            <a href="javascript:void(0);" class="pull-right" id="closeRight">
                <i class="fa fa-times"></i>
            </a>
        </h3>
        <div class="slimscroll">
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar2.png" alt="">
                <span>Sandra smith
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar3.png" alt="">
                <span>Amily Lee
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar4.png" alt="">
                <span>Christopher Palmer
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar5.png" alt="">
                <span>Nick Doe
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar2.png" alt="">
                <span>Sandra smith
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar3.png" alt="">
                <span>Amily Lee
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar4.png" alt="">
                <span>Christopher Palmer
                    <small>Hi! How're you?</small>
                </span>
            </a>
            <a href="javascript:void(0);" class="showRight2">
                <img src="assets/images/avatar5.png" alt="">
                <span>Nick Doe
                    <small>Hi! How're you?</small>
                </span>
            </a>
        </div>
    </nav>
    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
        <h3>
            <span class="pull-left">Sandra Smith</span>
            <a href="javascript:void(0);" class="pull-right" id="closeRight2">
                <i class="fa fa-angle-right"></i>
            </a>
        </h3>
        <div class="slimscroll chat">
            <div class="chat-item chat-item-left">
                <div class="chat-image">
                    <img src="assets/images/avatar2.png" alt="">
                </div>
                <div class="chat-message">
                    Hi There!
                </div>
            </div>
            <div class="chat-item chat-item-right">
                <div class="chat-message">
                    Hi! How are you?
                </div>
            </div>
            <div class="chat-item chat-item-left">
                <div class="chat-image">
                    <img src="assets/images/avatar2.png" alt="">
                </div>
                <div class="chat-message">
                    Fine! do you like my project?
                </div>
            </div>
            <div class="chat-item chat-item-right">
                <div class="chat-message">
                    Yes, It's clean and creative, good job!
                </div>
            </div>
            <div class="chat-item chat-item-left">
                <div class="chat-image">
                    <img src="assets/images/avatar2.png" alt="">
                </div>
                <div class="chat-message">
                    Thanks, I tried!
                </div>
            </div>
            <div class="chat-item chat-item-right">
                <div class="chat-message">
                    Good luck with your sales!
                </div>
            </div>
        </div>
        <div class="chat-write">
            <form class="form-horizontal" action="javascript:void(0);">
                <input type="text" class="form-control" placeholder="Say something">
            </form>
        </div>
    </nav>
    <div class="menu-wrap">
        <nav class="profile-menu">
            <div class="profile">
                <img src="assets/images/profile-menu-image.png" width="60" alt="Md Danish" />
                <span>Md Danish</span>
            </div>
            <div class="profile-menu-list">
                <a href="#">
                    <i class="fa fa-star"></i>
                    <span>Favorites</span>
                </a>
                <a href="#">
                    <i class="fa fa-bell"></i>
                    <span>Alerts</span>
                </a>
                <a href="#">
                    <i class="fa fa-envelope"></i>
                    <span>Messages</span>
                </a>
                <a href="#">
                    <i class="fa fa-comment"></i>
                    <span>Comments</span>
                </a>
            </div>
        </nav>
        <button class="close-button" id="close-button">Close Menu</button>
    </div>
    <form class="search-form no-print" action="#" method="GET">
        <div class="input-group">
            <input type="text" name="search" class="form-control search-input" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button">
                    <i class="fa fa-times"></i>
                </button>
            </span>
        </div>
        <!-- Input Group -->
    </form>
    <!-- Search Form -->
    <main class="page-content content-wrap">
        <div class="navbar">
            <div class="navbar-inner">
                <div class="sidebar-pusher">
                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="logo-box">
                    <a href="{{url('/')}}" class="logo-text">
                        <span>Admin Panel</span>
                    </a>
                </div>
                <!-- Logo Box -->
                <div class="search-button" style="display:none">
                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search">
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <div class="topmenu-outer">
                    <div class="top-menu">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen">
                                    <i class="fa fa-expand"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                    <i class="fa fa-dot-circle-o"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-md dropdown-list theme-settings" role="menu">
                                    <li class="li-group" style="display: none">
                                        <ul class="list-unstyled">
                                            <li class="no-link" role="presentation">
                                                Fixed Header
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right fixed-header-check" checked>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="li-group" style="display: none">
                                        <ul class="list-unstyled">
                                            <li class="no-link" role="presentation">
                                                Fixed Sidebar
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right fixed-sidebar-check" checked>
                                                </div>
                                            </li>
                                            <li class="no-link" role="presentation">
                                                Horizontal bar
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right horizontal-bar-check">
                                                </div>
                                            </li>
                                            <li class="no-link" role="presentation">
                                                Toggle Sidebar
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right toggle-sidebar-check">
                                                </div>
                                            </li>
                                            <li class="no-link" role="presentation">
                                                Compact Menu
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right compact-menu-check">
                                                </div>
                                            </li>
                                            <li class="no-link" role="presentation">
                                                Hover Menu
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right hover-menu-check">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="li-group" style="display: none">
                                        <ul class="list-unstyled">
                                            <li class="no-link" role="presentation">
                                                Boxed Layout
                                                <div class="ios-switch pull-right switch-md">
                                                    <input type="checkbox" class="js-switch pull-right boxed-layout-check">
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="li-group">
                                        <ul class="list-unstyled">
                                            <li class="no-link" role="presentation">
                                                Choose Theme Color
                                                <div class="color-switcher">
                                                    <a class="colorbox color-blue" href="?theme=blue" title="Blue Theme" data-css="blue"></a>
                                                    <a class="colorbox color-green" href="?theme=green" title="Green Theme" data-css="green"></a>
                                                    <a class="colorbox color-red" href="?theme=red" title="Red Theme" data-css="red"></a>
                                                    <a class="colorbox color-white" href="?theme=white" title="White Theme" data-css="white"></a>
                                                    <a class="colorbox color-purple" href="?theme=purple" title="purple Theme" data-css="purple"></a>
                                                    <a class="colorbox color-dark" href="?theme=dark" title="Dark Theme" data-css="dark"></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li style="display: none" class="no-link">
                                        <button class="btn btn-default reset-options">Reset Options</button>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li style="display:none">
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                    <i class="fa fa-envelope"></i>
                                    <span class="badge badge-success pull-right">4</span>
                                </a>
                                <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                    <li>
                                        <p class="drop-title">You have 4 new messages !</p>
                                    </li>
                                    <li class="dropdown-menu-list slimscroll messages">
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="#">
                                                    <div class="msg-img">
                                                        <div class="online on"></div>
                                                        <img class="img-circle" src="assets/images/avatar2.png" alt="">
                                                    </div>
                                                    <p class="msg-name">Sandra Smith</p>
                                                    <p class="msg-text">Hey ! I'm working on your project</p>
                                                    <p class="msg-time">3 minutes ago</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="msg-img">
                                                        <div class="online off"></div>
                                                        <img class="img-circle" src="assets/images/avatar4.png" alt="">
                                                    </div>
                                                    <p class="msg-name">Amily Lee</p>
                                                    <p class="msg-text">Hi David !</p>
                                                    <p class="msg-time">8 minutes ago</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="msg-img">
                                                        <div class="online off"></div>
                                                        <img class="img-circle" src="assets/images/avatar3.png" alt="">
                                                    </div>
                                                    <p class="msg-name">Christopher Palmer</p>
                                                    <p class="msg-text">See you soon !</p>
                                                    <p class="msg-time">56 minutes ago</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="msg-img">
                                                        <div class="online on"></div>
                                                        <img class="img-circle" src="assets/images/avatar5.png" alt="">
                                                    </div>
                                                    <p class="msg-name">Nick Doe</p>
                                                    <p class="msg-text">Nice to meet you</p>
                                                    <p class="msg-time">2 hours ago</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="msg-img">
                                                        <div class="online on"></div>
                                                        <img class="img-circle" src="assets/images/avatar2.png" alt="">
                                                    </div>
                                                    <p class="msg-name">Sandra Smith</p>
                                                    <p class="msg-text">Hey ! I'm working on your project</p>
                                                    <p class="msg-time">5 hours ago</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="msg-img">
                                                        <div class="online off"></div>
                                                        <img class="img-circle" src="assets/images/avatar4.png" alt="">
                                                    </div>
                                                    <p class="msg-name">Amily Lee</p>
                                                    <p class="msg-text">Hi David !</p>
                                                    <p class="msg-time">9 hours ago</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="drop-all">
                                        <a href="#" class="text-center">All Messages</a>
                                    </li>
                                </ul>
                            </li> -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                    <i class="fa fa-bell"></i>
                                    <span class="badge badge-success pull-right">3</span>
                                </a>
                                <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                    <li>
                                        <p class="drop-title">You have 3 pending tasks !</p>
                                    </li>
                                    <li class="dropdown-menu-list slimscroll tasks">
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="#">
                                                    <div class="task-icon badge badge-success">
                                                        <i class="icon-user"></i>
                                                    </div>
                                                    <span class="badge badge-roundless badge-default pull-right">1min ago</span>
                                                    <p class="task-details">New user registered.</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="task-icon badge badge-danger">
                                                        <i class="icon-energy"></i>
                                                    </div>
                                                    <span class="badge badge-roundless badge-default pull-right">24min ago</span>
                                                    <p class="task-details">Database error.</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="task-icon badge badge-info">
                                                        <i class="icon-heart"></i>
                                                    </div>
                                                    <span class="badge badge-roundless badge-default pull-right">1h ago</span>
                                                    <p class="task-details">Reached 24k likes</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="drop-all">
                                        <a href="#" class="text-center">All Tasks</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                    <!-- <span class="user-name">Danish</span> -->
                                    <img class="img-circle avatar" src="{{asset('assets/images/'.defaultAvatar(Auth::user()->gender))}}" width="40" height="40"
                                        alt="">
                                </a>
                                <ul class="dropdown-menu dropdown-list" role="menu">
                                    <li role="presentation">
                                        <a href="javascript:">
                                            <i class="fa fa-user"></i>Edit Profile</a>
                                    </li>
                                    <!-- <li role="presentation">
                                        <a href="inbox.html">
                                            <i class="fa fa-envelope"></i>Inbox
                                            <span class="badge badge-success pull-right">4</span>
                                        </a>
                                    </li> -->
                                    <li role="presentation">
                                        <a href="{{url('/change-password')}}">
                                            <i class="fa fa-lock"></i>Change Password</a>
                                    </li>
                                    <li role="presentation" class="divider"></li>
                                    <li role="presentation">
                                        <a href="{{url('/logout')}}">
                                            <i class="fa fa-sign-out m-r-xs"></i>Log out</a>
                                    </li>
                                </ul>
                            </li>
                            <!--<li>
                                    <a href="login.html" class="log-out waves-effect waves-button waves-classic">
                                        <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                    </a>
                                </li>-->
                            <li style="display: none">
                                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic" id="showRight">
                                    <i class="fa fa-comments"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Nav -->
                    </div>
                    <!-- Top Menu -->
                </div>
            </div>
        </div>
        <!-- Navbar -->
        <div class="page-sidebar sidebar">
            <div class="page-sidebar-inner slimscroll">
                <div class="sidebar-header">
                    <div class="sidebar-profile">
                        <!-- <a href="javascript:void(0);" id="profile-menu-link">
                            <div class="sidebar-profile-image">
                                <img src="assets/images/profile-menu-image.png" class="img-circle img-responsive" alt="">
                            </div>
                            <div class="sidebar-profile-details">
                                <span>Md Danish
                                    <br>
                                    <small>Administrator</small>
                                </span>
                            </div>
                        </a> -->
                        <div class="sidebar-profile-image">
                            <img src="{{asset('assets/images/'.defaultAvatar(Auth::user()->gender))}}" class="img-circle img-responsive" alt="">
                        </div>
                        <div class="sidebar-profile-details">
                            <span>{{Auth::user()->name}}
                                <br>
                                <small>Administrator</small>
                            </span>
                        </div>
                    </div>
                </div>
                <ul class="menu accordion-menu">

                    <li class="active">
                        <a href="{{url('/')}}" class="waves-effect waves-button">
                            <span class="menu-icon glyphicon glyphicon-home"></span>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="profile.html" class="waves-effect waves-button">
                            <span class="menu-icon glyphicon glyphicon-user"></span>
                            <p>Profile</p>
                        </a>
                    </li> -->
                    <li class="droplink">
                        <a href="#" class="waves-effect waves-button">
                            <span class="menu-icon glyphicon glyphicon-folder-close"></span>
                            <p>Manage</p>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{url('/towers')}}">Towers</a>
                            </li>
                            <li>
                                <a href="{{url('/floors')}}">Floors</a>
                            </li>
                            <li>
                                <a href="{{url('/flats')}}">Flats</a>
                            </li>
                            <li>
                                <a href="{{url('/flat-types')}}">Flat Types</a>
                            </li>
                            <li>
                                <a href="{{url('/owners')}}">Owners</a>
                            </li>
                            <li>
                                <a href="{{url('/renters')}}">Renters</a>
                            </li>
                            <li>
                                <a href="{{url('/opening-balances')}}">Opening Balances</a>
                            </li>
                            <li>
                                <a href="{{url('/parkings')}}">Parkings</a>
                            </li>
                            <li>
                                <a href="{{url('/taxes')}}">Taxes</a>
                            </li>
                        </ul>
                    </li>
                    <li class="droplink">
                        <a href="#" class="waves-effect waves-button">
                            <span class="menu-icon glyphicon glyphicon-credit-card"></span>
                            <p>Maintenance</p>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{url('/maintenance-rates')}}">Maintenance Rates</a>
                            </li>
                            <li>
                                <a href="{{url('/maintenance-dues')}}">Maintenance Dues</a>
                            </li>
                            <li>
                                <a href="{{url('/maintenance-advances')}}">Advance Payment</a>
                            </li>
                        </ul>
                    </li>
                    <li class="droplink">
                        <a href="#" class="waves-effect waves-button">
                            <span class="menu-icon glyphicon glyphicon-flash"></span>
                            <p>Electricity</p>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{url('/fixed-loads')}}">Fixed Loads</a>
                            </li>
                            <li>
                                <a href="{{url('/fixed-rates')}}">Fixed Rates</a>
                            </li>
                            <li>
                                <a href="{{url('/opening-readings')}}">Opening Readings</a>
                            </li>
                            <li>
                                <a href="{{url('/electricity-bills')}}">Electricity Bills</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('/error-logs')}}" class="waves-effect waves-button">
                            <span class="menu-icon glyphicon glyphicon-warning-sign"></span>
                            <p>Error Logs</p>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Page Sidebar Inner -->
        </div>
        <!-- Page Sidebar -->
        <div class="page-inner">
            @yield('content')
            <div class="page-footer">
                <p class="no-s">2018 &copy; Techstreet Solutions Pvt. Ltd.</p>
            </div>
        </div>
        <!-- Page Inner -->
    </main>
    <!-- Page Content -->


    <!-- Javascripts -->
    <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/plugins/pace-master/pace.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/plugins/switchery/switchery.min.js')}}"></script>
    <script src="{{asset('assets/plugins/uniform/jquery.uniform.min.js')}}"></script>
    <script src="{{asset('assets/plugins/offcanvasmenueffects/js/classie.js')}}"></script>
    <!-- <script src="{{asset('assets/plugins/offcanvasmenueffects/js/main.js')}}"></script> -->
    <script src="{{asset('assets/plugins/waves/waves.min.js')}}"></script>
    <script src="{{asset('assets/plugins/3d-bold-navigation/js/main.js')}}"></script>
    <script src="{{asset('assets/js/modern.js')}}"></script>
    <!-- Data Table -->
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
            $('#datePicker').datepicker({
                calendarWeeks: true,
                todayHighlight: true,
                autoclose: true,
                format: "dd-MM-yyyy",
            });
        });
    </script>

</body>

</html>