@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Add Floor</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/floors')}}">Floors</a>
            </li>
            <li class="active">Add</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">

                    <form>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="tower_id">*Tower</label>
                                <select class="form-control" name="tower_id" id="tower_id" required>
                                    @foreach($towers as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4" style="margin-bottom: 0px">
                                <label for="floor_number">*No. of Floors</label>
                                <input type="number" class="form-control" name="floor_number" id="floor_number" min="1" placeholder="Enter number of floors"
                                    required>
                            </div>
                            <div class="form-group col-md-4" style="margin-top: 23px">
                                <button type="button" class="btn btn-info" id="floorAddBtn">Add</button>
                            </div>
                        </div>
                    </form>

                    <div class="floorFormWrapper">

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#floorAddBtn').click(function (event) {
            $('.floorFormWrapper').html('');
            var tower = $('#tower_id option:selected').val();
            var floor = $('#floor_number').val();
            $('.floorFormWrapper').append('<form action="{{ route("floors.store") }}" method="post" class="form-inline floorFormBody">' +
                '{{ csrf_field() }}' +
                '<input type="hidden" name="tower_id" value="' + tower + '"/>' +
                '</form>');
            for (var i = 0; i < floor; i++) {
                $('.floorFormBody').append('<div class="form-group" style="margin:5px 5px 5px 0px">' +
                    '<input type="text" class="form-control" name="floor_number[]" value="'+i+'" placeholder="Enter floor number" required>' +
                    '</div>');
            }
            $('.floorFormBody').append('<button style="display:block; margin-top:15px" type="submit" class="btn btn-primary">Submit</button>');
        });
    });
</script> @endsection