@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Fixed Load</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/fixed-loads')}}">Fixed Loads</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Flat Type</label>
                            <p>{{$FixedLoad->flat_type_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Load</label>
                            <p>{{$FixedLoad->capacity}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Comments</label>
                            <p>{{noneIfEmpty($FixedLoad->comments)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection