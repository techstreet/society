@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Fixed Load</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/fixed-loads')}}">Fixed Loads</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('fixed-loads.update', $FixedLoad->id) }}" method="post">
                        {{ method_field('put') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="flat_type_id">*Flat Type</label>
                            <select class="form-control" name="flat_type_id" required>
                                @foreach($flat_types as $value)
                                <option value="{{$value->id}}" @if($FixedLoad->flat_type_id == $value->id) selected @endif>{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="capacity">*Load (kW)</label>
                            <input type="number" class="form-control" name="capacity" value="{{$FixedLoad->capacity}}" min="0" step="0.01" placeholder="Enter load" required>
                        </div>
                        <div class="form-group">
                            <label for="comments">Comments</label>
                            <textarea class="form-control" name="comments" placeholder="Enter comments">{{$FixedLoad->comments}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection