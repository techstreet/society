@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Tower</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/towers')}}">Towers</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="text-success">Name</label>
                            <p>{{$tower->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <label class="text-success">Description</label>
                            <p>{{noneIfEmpty($tower->description)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection