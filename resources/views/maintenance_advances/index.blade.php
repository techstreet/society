@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Advance Payment</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">Advance Payment</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-heading mb-15 clearfix">
                    <a class="btn btn-info btn-addon btn-sm pull-right" href="{{url('/maintenance-advances/create')}}">
                        <i class="fa fa-plus"></i>Add New</a>
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tower</th>
                                <th>Floor</th>
                                <th>Flat</th>
                                <th>Amount</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($maintenance_advances as $key=>$value)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td>{{$value->tower_name}}</td>
                                <td>{{$value->floor_name}}</td>
                                <td>{{$value->flat_name}}</td>
                                <td>{{$value->amount}}</td>
                                <td>{{$value->comments}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection