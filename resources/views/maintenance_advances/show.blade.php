@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Renter</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/renters')}}">Renters</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-8">
            @include('layouts.flash_message')
            <div class="panel panel-white">
            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Tower</label>
                            <p>{{$renter->tower_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Floor</label>
                            <p>{{$renter->floor_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Flat</label>
                            <p>{{$renter->flat_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Name</label>
                            <p>{{$renter->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Mobile</label>
                            <p>{{$renter->mobile}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Email</label>
                            <p>{{$renter->email}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Address</label>
                            <p>{{$renter->address}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection