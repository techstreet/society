@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Add Advance Payment</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/maintenance-advances')}}">Advance Payment</a>
            </li>
            <li class="active">Add</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('maintenance-advances.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">*Tower</label>
                                    <select class="form-control" name="tower_id" id="towerOptions" onChange="getFloors(this.value);" required>
                                        <option value="">Select</option>
                                        @foreach($towers as $value)
                                        <option value="{{$value->tower_id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Floor</label>
                                    <select class="form-control" name="floor_id" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" onChange="getOwner(document.getElementById('towerOptions').value,document.getElementById('floorOptions').value,this.value);" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <div id="ownerInput">
                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="month">*Advance Upto</label>
                                    <select class="form-control" name="month" id="monthOptions" required>
                                        <option value="">Select Month</option>
                                        <option value='1'>January</option>
                                        <option value='2'>February</option>
                                        <option value='3'>March</option>
                                        <option value='4'>April</option>
                                        <option value='5'>May</option>
                                        <option value='6'>June</option>
                                        <option value='7'>July</option>
                                        <option value='8'>August</option>
                                        <option value='9'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year">&nbsp;</label>
                                    <select class="form-control" name="year" id="yearOptions" onChange="calAdvance(document.getElementById('flatOptions').value,document.getElementById('monthOptions').value,this.value);" required>
                                        <option value="">Select Year</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="amountInput">
                            
                        </div>
                        <div class="form-group">
                            <label for="comments">Remarks</label>
                            <textarea class="form-control" name="comments" placeholder="Enter remarks"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getOwner(val1, val2, val3) {
        $.ajax({
            type: "GET",
            url: "{{url('/input/owner3')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2 +'&flat_id=' + val3,
            success: function (data) {
                $("#ownerInput").html('');
                $("#ownerInput").html(data);
            }
        });
    }
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
    function calAdvance(val1,val2,val3) {
        $.ajax({
            type: "GET",
            url: "{{url('/calculate/advance')}}",
            data: 'flat_id=' + val1 + '&month=' + val2 + '&year=' + val3,
            success: function (data) {
                $("#amountInput").html('');
                $("#amountInput").html(data);
            }
        });
    }
</script> @endsection