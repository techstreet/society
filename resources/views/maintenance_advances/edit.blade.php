@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Renter</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/renters')}}">Renters</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('renters.update', $renter->id) }}" method="post">
                    {{ method_field('put') }} {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">*Tower</label>
                                    <select class="form-control" name="tower_id" id="towerOptions" onChange="getFloors(this.value);" required>
                                        @foreach($towers as $value)
                                        <option value="{{$value->tower_id}}" @if($renter->tower_id == $value->tower_id) selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Floor</label>
                                    <select class="form-control" name="floor_id" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);" required>
                                        @foreach($floors as $value)
                                        <option value="{{$value->id}}" @if($renter->floor_id == $value->id) selected @endif>{{$value->floor_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" required>
                                        @foreach($flats as $value)
                                        <option value="{{$value->id}}" @if($renter->flat_id == $value->id) selected @endif>{{$value->flat_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group">
                            <label for="name">*Name</label>
                            <input type="text" class="form-control" name="name" value="{{$renter->name}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="mobile">*Mobile</label>
                            <input type="number" class="form-control" name="mobile" value="{{$renter->mobile}}" placeholder="Enter mobile" required>
                        </div>
                        <div class="form-group">
                            <label for="email">*Email</label>
                            <input type="email" class="form-control" name="email" value="{{$renter->email}}" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="address">*Address</label>
                            <textarea class="form-control" name="address" placeholder="Enter address" required>{{$renter->address}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
</script> @endsection