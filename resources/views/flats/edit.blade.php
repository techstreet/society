@extends('layouts.app') @section('content')
<div class="page-title">
<h3>{{$flats[0]->tower_name}} &amp; Floor - {{$flats[0]->floor_name}}</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/flats')}}">Flats</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form class="form-inline" action="{{ route('flats.update', $flats[0]->id) }}" method="post">
                        {{ method_field('put') }} {{ csrf_field() }}
                        @foreach($flats as $value)
                        <div class="form-group" style="margin:5px 5px 5px 0px">
                            <input type="text" class="form-control" name="flat_number[]" value="{{$value->flat_number}}" placeholder="Enter flat number" required>
                        </div>
                        @endforeach
                        <button style="display: block; margin-top: 15px" type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection