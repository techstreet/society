@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>{{$flats[0]->tower_name}} &amp; Floor - {{$flats[0]->floor_name}}</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/flats')}}">Flats</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($flats as $value)
                            <span class="badge badge-primary">Flat No - {{$value->flat_number}}</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection