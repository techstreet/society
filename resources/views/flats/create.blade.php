@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Add Flat</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/flats')}}">Flats</a>
            </li>
            <li class="active">Add</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">

                    <form action="{{ route('flats.store') }}" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="tower_id">*Tower</label>
                            <select class="form-control" name="tower_id" id="tower_id" onChange="getFloors(this.value);" required>
                            <option value="">Select</option>    
                            @foreach($towers as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="chkWrapper" style="display:none">
                            <label for="floor"><b>Floor Number</b></label>
                            <input type="checkbox" id="selectAll" checked/> Select All
                        </div>
                        <div class="form-group" id="chkBody">
                            
                        </div>
                        <div class="form-group">
                            <label for="number_flats">*Number of Flats</label>
                            <input class="form-control" type="number" name="number_flats" min="0"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                    <div class="flatFormWrapper">

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#flatAddBtn').click(function (event) {
            $('.flatFormWrapper').html('');
            var tower = $('#tower_id option:selected').val();
            var flat = $('#flat_number').val();
            $('.flatFormWrapper').append('<form action="{{ route("flats.store") }}" method="post" class="form-inline flatFormBody">' +
                '{{ csrf_field() }}' +
                '<input type="hidden" name="tower_id" value="' + tower + '"/>' +
                '</form>');
            for (var i = 0; i < flat; i++) {
                $('.flatFormBody').append('<div class="form-group" style="margin:5px 5px 5px 0px">' +
                    '<input type="text" class="form-control" name="flat_number[]" value="'+i+'" placeholder="Enter flat number" required>' +
                    '</div>');
            }
            $('.flatFormBody').append('<button style="display:block; margin-top:15px" type="submit" class="btn btn-primary">Submit</button>');
        });
    });

    function getFloors(val) {
        $.ajax({
        type: "GET",
        url: "{{url('/checkbox/floors')}}",
        data:'tower_id='+val,
        success: function(data){
            $("#chkBody").html('');
            $("#chkBody").html(data);
            $("#chkWrapper").show();
        }
        });
    }

    $('#selectAll').click(function() {
		if ($(this).is(':checked')) {
			$(".floorChkbox").prop("checked", true);
		}
		else {
			$(".floorChkbox").prop("checked", false);
		}
	});

</script> @endsection