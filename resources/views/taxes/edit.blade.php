@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Tax</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/taxes')}}">Taxes</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('taxes.update', $tax->id) }}" method="post">
                        {{ method_field('put') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">*Name</label>
                            <input type="text" class="form-control" name="name" value="{{$tax->name}}" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="rate">*Rate(%)</label>
                            <input type="number" class="form-control" name="rate" value="{{$tax->rate}}" min="0" step="0.01" placeholder="Enter rate" required>
                        </div>
                        <div class="form-group">
                            <label for="status">*Status</label>
                            <select name="is_active" class="form-control" required>
                                <option value="1" @if($tax->is_active == '1') selected @endif>Active</option>
                                <option value="0" @if($tax->is_active == '0') selected @endif>Inactive</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection