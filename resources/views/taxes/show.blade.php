@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Tax</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/taxes')}}">Taxes</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Name</label>
                            <p>{{$tax->name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Rate(%)</label>
                            <p>{{$tax->rate}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Status</label>
                            <p>@if($tax->is_active == '1') Active @else Inactive @endif</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection