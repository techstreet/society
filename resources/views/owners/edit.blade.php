@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Owner</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/owners')}}">Owners</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('owners.update', $owner->id) }}" method="post">
                        {{ method_field('put') }} {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name">*Tower</label>
                                    <select class="form-control" name="tower_id" id="towerOptions" onChange="getFloors(this.value);" required>
                                        @foreach($towers as $value)
                                        <option value="{{$value->id}}" @if($owner->tower_id == $value->id) selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="floor">*Floor</label>
                                    <select class="form-control" name="floor_id" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);" required>
                                        @foreach($floors as $value)
                                        <option value="{{$value->id}}" @if($owner->floor_id == $value->id) selected @endif>{{$value->floor_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="floor">*Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" required>
                                        @foreach($flats as $value)
                                        <option value="{{$value->id}}" @if($owner->flat_id == $value->id) selected @endif>{{$value->flat_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="floor">*Flat Type</label>
                                        <select class="form-control" name="flat_type_id" required>
                                            @foreach($flat_types as $value)
                                            <option value="{{$value->id}}" @if($owner->flat_type_id == $value->id) selected @endif>{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group">
                            <label for="first_owner">*First Owner</label>
                            <input type="text" class="form-control" name="first_owner" value="{{$owner->first_owner}}" placeholder="Enter first owner name"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="second_owner">Second Owner</label>
                            <input type="text" class="form-control" name="second_owner" value="{{$owner->second_owner}}" placeholder="Enter second owner name">
                        </div>
                        <div class="form-group">
                            <label for="third_owner">Third Owner</label>
                            <input type="text" class="form-control" name="third_owner" value="{{$owner->third_owner}}" placeholder="Enter third owner name">
                        </div>
                        <div class="form-group">
                            <label for="mobile">*Mobile</label>
                            <input type="number" class="form-control" name="mobile" value="{{$owner->mobile}}" placeholder="Enter mobile" required>
                        </div>
                        <div class="form-group">
                            <label for="email">*Email</label>
                            <input type="email" class="form-control" name="email" value="{{$owner->email}}" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="address">*Address</label>
                            <textarea class="form-control" name="address" placeholder="Enter address" required>{{$owner->address}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors1')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats1')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
</script> @endsection