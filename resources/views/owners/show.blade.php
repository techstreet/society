@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Owner</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/owners')}}">Owners</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Tower</label>
                            <p>{{$owner->tower_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Floor</label>
                            <p>{{$owner->floor_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Flat</label>
                            <p>{{$owner->flat_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Flat Type</label>
                            <p>{{$owner->flat_type}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">First Owner</label>
                            <p>{{$owner->first_owner}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Second Owner</label>
                            <p>{{noneIfEmpty($owner->second_owner)}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Third owner</label>
                            <p>{{noneIfEmpty($owner->third_owner)}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Mobile</label>
                            <p>{{$owner->mobile}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Email</label>
                            <p>{{$owner->email}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Address</label>
                            <p>{{$owner->address}}</p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection