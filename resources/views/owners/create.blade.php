@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Add Owner</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/owners')}}">Owners</a>
            </li>
            <li class="active">Add</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-8">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('owners.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name">*Tower</label>
                                    <select class="form-control" name="tower_id" id="towerOptions" onChange="getFloors(this.value);" required>
                                        <option value="">Select</option>
                                        @foreach($towers as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="floor">*Floor</label>
                                    <select class="form-control" name="floor_id" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);"
                                        required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="floor">*Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="floor">*Flat Type</label>
                                    <select class="form-control" name="flat_type_id" required>
                                        <option value="">Select</option>
                                        @foreach($flat_types as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group">
                            <label for="first_owner">*First Owner</label>
                            <input type="text" class="form-control" name="first_owner" placeholder="Enter first owner name" required>
                        </div>
                        <div class="form-group">
                            <label for="second_owner">Second Owner</label>
                            <input type="text" class="form-control" name="second_owner" placeholder="Enter second owner name">
                        </div>
                        <div class="form-group">
                            <label for="third_owner">Third Owner</label>
                            <input type="text" class="form-control" name="third_owner" placeholder="Enter third owner name">
                        </div>
                        <div class="form-group">
                            <label for="mobile">*Mobile</label>
                            <input type="number" class="form-control" name="mobile" placeholder="Enter mobile" required>
                        </div>
                        <div class="form-group">
                            <label for="email">*Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="address">*Address</label>
                            <textarea class="form-control" name="address" placeholder="Enter address" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors1')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats1')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
</script> @endsection