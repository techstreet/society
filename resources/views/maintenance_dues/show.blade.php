@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Maintenance Due</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/maintenance-rates')}}">Maintenance Dues</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-success">Flat Type</label>
                            <p>{{$MaintenanceDue->flat_type_name}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Type</label>
                            <p>
                                @if($MaintenanceDue->type == '1') Fixed @endif @if($MaintenanceDue->type == '2') Per Square Feet @endif
                            </p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Rate</label>
                            <p>{{noneIfEmpty($MaintenanceDue->rate)}}</p>
                        </div>
                        <div class="col-md-3">
                            <label class="text-success">Applicable From</label>
                            <p>{{date_dfy($MaintenanceDue->applicable_from)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection