@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Opening Balance</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/opening-balances')}}">Opening Balances</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-8">
            @include('layouts.flash_message')
            <div class="panel panel-white">
            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Tower</label>
                            <p>{{$opening_balance->tower_name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Floor</label>
                            <p>{{$opening_balance->floor_name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Flat</label>
                            <p>{{$opening_balance->flat_name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Amount</label>
                            <p>{{$opening_balance->debit}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Date</label>
                            <p>{{date_dfy($opening_balance->entry_date)}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Remarks</label>
                            <p>{{noneIfEmpty($opening_balance->comments)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection