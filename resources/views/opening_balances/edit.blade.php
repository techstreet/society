@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Edit Opening Balance</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/opening-balances')}}">Opening Balances</a>
            </li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('opening-balances.update', $opening_balance->id) }}" method="post">
                    {{ method_field('put') }} {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">*Tower</label>
                                    <select class="form-control" name="tower_id" id="towerOptions" onChange="getFloors(this.value);" required>
                                        @foreach($towers as $value)
                                        <option value="{{$value->tower_id}}" @if($opening_balance->tower_id == $value->tower_id) selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Floor</label>
                                    <select class="form-control" name="floor_id" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);" required>
                                        @foreach($floors as $value)
                                        <option value="{{$value->floor_id}}" @if($opening_balance->floor_id == $value->floor_id) selected @endif>{{$value->floor_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="floor">*Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" required>
                                        @foreach($flats as $value)
                                        <option value="{{$value->flat_id}}" @if($opening_balance->flat_id == $value->flat_id) selected @endif>{{$value->flat_number}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
                        <div class="form-group">
                            <label for="debit">*Amount</label>
                            <input type="number" class="form-control" name="debit" value="{{$opening_balance->debit}}" step="0.01" placeholder="Enter amount" required>
                        </div>
                        <div class="form-group">
                            <label for="entry_date">*Date</label>
                            <input type="text" class="form-control" id="datePicker" name="entry_date" value="{{date_dfy($opening_balance->entry_date)}}" placeholder="Enter date" required>
                        </div>
                        <div class="form-group">
                            <label for="comments">Remarks</label>
                            <textarea class="form-control" name="comments" placeholder="Enter remarks">{{$opening_balance->comments}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
</script> @endsection