@extends('layouts.app') @section('content')
<div class="page-title">
  <h3>Error Logs</h3>
  <div class="page-breadcrumb">
    <ol class="breadcrumb">
      <li>
        <a href="index.html">Home</a>
      </li>
      <li class="active">Error Logs</li>
    </ol>
  </div>
</div>
<div id="main-wrapper">
  <div class="row">
    <div class="col-12 container">
      @if ($logs === null)
      <div>
        Log file >50M, please download it.
      </div>
      @else
      <table id="table-log" class="table table-striped">
        <thead>
          <tr>
            <th>Level</th>
            <th>Context</th>
            <th>Date</th>
            <th>Content</th>
          </tr>
        </thead>
        <tbody>
          @foreach($logs as $key => $log)
          <tr data-display="stack{{{$key}}}">
            <td class="text-{{{$log['level_class']}}}" style="min-width: 75px;">
              <span class="fa fa-{{{$log['level_img']}}}" aria-hidden="true"></span> &nbsp;{{$log['level']}}</td>
            <td class="text" style="min-width: 75px;">{{$log['context']}}</td>
            <td class="date" style="min-width: 75px;">{{{$log['date']}}}</td>
            <td class="text">
              @if ($log['stack'])
              <button type="button" class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2" data-display="stack{{{$key}}}">
                <span class="fa fa-search"></span>
              </button>@endif {{{$log['text']}}} @if (isset($log['in_file']))
              <br/>{{{$log['in_file']}}}@endif @if ($log['stack'])
              <div class="stack" id="stack{{{$key}}}" style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
              </div>@endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @endif
      <div class="p-3">
        @if($current_file)
        <a href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}">
          <span class="fa fa-download"></span>
          Download file</a>
        -
        <a id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}">
          <span class="fa fa-trash"></span> Delete file</a>
        @if(count($files) > 1) -
        <a id="delete-all-log" href="?delall=true">
          <span class="fa fa-trash"></span> Delete all files</a>
        @endif @endif
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    $('.table-container tr').on('click', function () {
      $('#' + $(this).data('display')).toggle();
    });
    $('#table-log').DataTable({
      "order": [1, 'desc'],
      "stateSave": true,
      "stateSaveCallback": function (settings, data) {
        window.localStorage.setItem("datatable", JSON.stringify(data));
      },
      "stateLoadCallback": function (settings) {
        var data = JSON.parse(window.localStorage.getItem("datatable"));
        if (data) data.start = 0;
        return data;
      }
    });
    $('#delete-log, #delete-all-log').click(function () {
      return confirm('Are you sure?');
    });
  });
</script> @endsection