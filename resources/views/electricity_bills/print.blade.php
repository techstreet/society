@extends('layouts.app') @section('content')
<div class="page-title no-print">
    <h3>Print Electricity Bill</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/electricity-bills')}}">Electricity Bills</a>
            </li>
            <li class="active">Print</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.flash_message')
            <div class="panel panel-white">
            <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-9">
                            <h1 class="m-b-md"><b>Society</b></h1>
                            <address>
                                Melbourne, Australia<br>
                                P: (123) 456-7890
                            </address>
                        </div>
                        <div class="col-xs-3">
                                <h1>INVOICE</h1>
                                <address>
                                Invoice No: {{$electricity_bill->id}}<br>
                                Date: {{date_dfy($electricity_bill->entry_date)}}
                                </address>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-12">
                            <address>
                            <strong>Invoice to:</strong><br/>
                                {{$owner->first_owner}}<br>
                                {{$owner->address}}
                            </address>
                        </div>
                    </div>
                    <div class="row">
                    <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th rowspan="2">S.NO</th>
                            <th rowspan="2">Particular</th>
                            <th rowspan="2">HSN/SAC</th>
                            <th rowspan="2">Quantity</th>
                            <th rowspan="2">Rate</th>
                            <th colspan="2">CGST</th>
                            <th colspan="2">SGST</th>
                            <th rowspan="2">Total</th>
                        </tr>
                        <tr>
                            <th>Rate (%)</th>
                            <th>Amount</th>
                            <th>Rate (%)</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><p><strong>Electricity Charges</strong></p>
                            <div class="row">
                                <div class="col-md-4">
                                    <p>Opening</p>
                                    <p>{{zeroIfEmpty($electricity_bill->opening_reading)}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>Closing</p>
                                    <p>{{zeroIfEmpty($electricity_bill->closing_reading)}}</p>
                                </div>
                                <div class="col-md-4">
                                    <p>Units</p>
                                    <p>{{zeroIfEmpty($electricity_bill->closing_reading-$electricity_bill->opening_reading)}}</p>
                                </div>
                            </div>
                            </td>
                            <td>84439990</td>
                            <td>{{zeroIfEmpty($electricity_bill->closing_reading-$electricity_bill->opening_reading)}}</td>
                            <td>{{zeroIfEmpty($electricity_bill->unit_charge)}}</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>{{zeroIfEmpty(($electricity_bill->closing_reading-$electricity_bill->opening_reading)*$electricity_bill->unit_charge)}}</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><p><strong>Load Charges</strong></p>
                            </td>
                            <td>84439990</td>
                            <td>{{zeroIfEmpty($electricity_bill->capacity)}}</td>
                            <td>{{zeroIfEmpty($electricity_bill->capacity_charge)}}</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>{{zeroIfEmpty($electricity_bill->capacity*$electricity_bill->capacity_charge)}}</td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align:right">Total Amount Before Tax:</td>
                            <td>{{zeroIfEmpty(($electricity_bill->closing_reading-$electricity_bill->opening_reading)*$electricity_bill->unit_charge)+zeroIfEmpty($electricity_bill->capacity*$electricity_bill->capacity_charge)}}</td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align:right">CGST:</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align:right">SGST:</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align:right">Total Amount:</td>
                            <td>{{zeroIfEmpty(($electricity_bill->closing_reading-$electricity_bill->opening_reading)*$electricity_bill->unit_charge)+zeroIfEmpty($electricity_bill->capacity*$electricity_bill->capacity_charge)}}</td>
                        </tr>
                    </tbody>
                    </table>
                    <button style="float:right" type="button" class="btn btn-lg btn-default print no-print"><i class="fa fa-print"></i> Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$(".print").click(function(){
    printPage();
});

function printPage() {
    window.print();
}
</script>

@endsection