@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Electricity Bill</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/electricity-bills')}}">Electricity Bills</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-8">
            @include('layouts.flash_message')
            <div class="panel panel-white">
            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Tower</label>
                            <p>{{$electricity_bill->tower_name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Floor</label>
                            <p>{{$electricity_bill->floor_name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Flat</label>
                            <p>{{$electricity_bill->flat_name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Opening Reading</label>
                            <p>{{zeroIfEmpty($electricity_bill->opening_reading)}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Closing Reading</label>
                            <p>{{zeroIfEmpty($electricity_bill->closing_reading)}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Date</label>
                            <p>{{date_dfy($electricity_bill->entry_date)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection