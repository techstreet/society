@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Account History</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/electricity-bills')}}">Electricity Bills</a>
            </li>
            <li class="active">Account History</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-heading mb-15 clearfix">
                    <a class="btn btn-info btn-addon btn-sm pull-right" href="{{url('/electricity-bills/receive-payment/'.$flat_id)}}">
                        <i class="fa fa-money"></i>Receive Payment</a>
                </div>
                <div class="panel-body">
                    <table class="table table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Particular</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Balance</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($flats as $key=>$value)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td>{{$value->comments}}</td>
                                <td>{{$value->debit}}</td>
                                <td>{{$value->credit}}</td>
                                <td>{{$value->balance}}</td>
                                <td>{{date_dfy($value->entry_date)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection