@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Electricity Bills</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">Electricity Bills</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-heading mb-15 clearfix">
                    <a class="btn btn-info btn-addon btn-sm pull-right" href="{{url('/electricity-bills/create')}}">
                        <i class="fa fa-plus"></i>Add New</a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="name">Tower</label>
                                    <select class="form-control" id="towerOptions" onChange="getFloors(this.value);" required>
                                        <option value="">Select</option>
                                        @foreach($towers as $value)
                                        <option value="{{$value->tower_id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <label for="floor">Floor</label>
                                    <select class="form-control" id="floorOptions" onChange="getFlats(document.getElementById('towerOptions').value,this.value);" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <label for="floor">Flat</label>
                                    <select class="form-control" name="flat_id" id="flatOptions" required>
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button style="margin-top:23px" type="submit" class="btn btn-primary">Find</button>
                            </div>
                        </form>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tower</th>
                                <th>Floor</th>
                                <th>Flat</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($count>0)
                            @foreach($electricity_bills as $key=>$value)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td>{{$value->tower_name}}</td>
                                <td>{{$value->floor_name}}</td>
                                <td>{{$value->flat_name}}</td>
                                <td>{{$value->debit_total-$value->credit_total}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{url('/electricity-bills/print/'.$value->id)}}" class="btn btn-primary">
                                            <i class="fa fa-print"></i> Print </a>
                                            <a href="{{url('/electricity-bills/account/'.$value->flat_id)}}" class="btn btn-success">
                                            <i class="fa fa-history"></i> Account History </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="6">No Records Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getFloors(val) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/floors')}}",
            data: 'tower_id=' + val,
            success: function (data) {
                $("#floorOptions").html('');
                $("#flatOptions").html('<option value="">Select</option>');
                $("#floorOptions").html(data);
            }
        });
    }
    function getFlats(val1, val2) {
        $.ajax({
            type: "GET",
            url: "{{url('/dropdown/flats')}}",
            data: 'tower_id=' + val1 + '&floor_id=' + val2,
            success: function (data) {
                $("#flatOptions").html('');
                $("#flatOptions").html(data);
            }
        });
    }
</script> @endsection