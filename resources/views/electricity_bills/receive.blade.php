@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Receive Payment</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/electricity-bills')}}">Electricity Bills</a>
            </li>
            <li>
                <a href="{{url('/electricity-bills/account/'.$flat_id)}}">Account History</a>
            </li>
            <li class="active">Receive Payment</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="credit">*Amount</label>
                            <input type="number" class="form-control" name="credit" min="0" step="0.01" placeholder="Enter amount" required>
                        </div>
                        <div class="form-group">
                            <label for="entry_date">*Date</label>
                            <input type="text" class="form-control" name="entry_date" id="datePicker" placeholder="Enter date" required>
                        </div>
                        <div class="form-group">
                            <label for="comments">*Remarks</label>
                            <textarea class="form-control" name="comments" placeholder="Enter remarks" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection