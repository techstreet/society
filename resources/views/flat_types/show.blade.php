@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>View Flat Type</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/flat-types')}}">Flat Types</a>
            </li>
            <li class="active">View</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-9">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Name</label>
                            <p>{{$FlatType->name}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Rooms</label>
                            <p>{{noneIfEmpty($FlatType->rooms)}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Wash Rooms</label>
                            <p>{{noneIfEmpty($FlatType->wash_rooms)}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="text-success">Store Rooms</label>
                            <p>{{$FlatType->store_rooms}}</p>
                        </div>
                        <div class="col-md-4">
                            <label class="text-success">Area</label>
                            <p>{{noneIfEmpty($FlatType->area)}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection