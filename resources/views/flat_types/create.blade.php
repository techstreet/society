@extends('layouts.app') @section('content')
<div class="page-title">
    <h3>Add Flat Types</h3>
    <div class="page-breadcrumb">
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/flat-types')}}">Flat Types</a>
            </li>
            <li class="active">Add</li>
        </ol>
    </div>
</div>
<div id="main-wrapper">
    <div class="row">
        <div class="col-md-6">
            @include('layouts.flash_message')
            <div class="panel panel-white">
                <div class="panel-body">
                    <form action="{{ route('flat-types.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">*Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="rooms">*Rooms</label>
                            <input type="number" class="form-control" name="rooms" min="0" placeholder="Enter number of rooms" required>
                        </div>
                        <div class="form-group">
                            <label for="wash_rooms">*Washrooms</label>
                            <input type="number" class="form-control" name="wash_rooms" min="0" placeholder="Enter number of wash rooms" required>
                        </div>
                        <div class="form-group">
                            <label for="store_rooms">*Store/Servant Rooms</label>
                            <input type="number" class="form-control" name="store_rooms" min="0" placeholder="Enter number of store rooms" required>
                        </div>
                        <div class="form-group">
                            <label for="area">*Area/Size</label>
                            <input type="text" class="form-control" name="area" placeholder="Enter area" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection