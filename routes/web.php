<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::check()){
		return view('welcome');
	}
	else{
		return redirect()->action('LoginController@index');
	}
});

Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@index']);
Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@login']);

Route::group(['middleware' => 'auth'], function () {
	Route::get('/test','TestController@index');
	//Logs
	Route::get('error-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
	//Change password
	Route::get('/change-password','ChangepasswordController@index');
	Route::post('/change-password','ChangepasswordController@save');
	//Tower
	Route::resource('towers', 'TowerController');
	//Floors
	Route::resource('floors', 'FloorController');
	//Flats
	Route::resource('flats', 'FlatController');
	//Flat Type
	Route::resource('flat-types', 'FlatTypeController');
	//Owners
	Route::resource('owners', 'OwnerController');
	//Renters
	Route::resource('renters', 'RenterController');
	//Parkings
	Route::resource('parkings', 'ParkingController');
	//Tax
	Route::resource('taxes', 'TaxController');
	//Maintenance Rate
	Route::resource('maintenance-rates', 'MaintenanceRateController');
	//Advance Payment
	Route::resource('maintenance-advances', 'MaintenanceAdvanceController');
	//Maintenance Dues
	Route::resource('maintenance-dues', 'MaintenanceDueController');
	Route::get('/maintenance-dues/account/{id}','MaintenanceDueController@accountHistory');
	Route::get('/maintenance-dues/receive-payment/{id}','MaintenanceDueController@getReceive');
	Route::post('/maintenance-dues/receive-payment/{id}','MaintenanceDueController@postReceive');
	//Opening Balance
	Route::resource('opening-balances', 'OpeningBalanceController');
	//Opening Reading
	Route::resource('opening-readings', 'OpeningReadingController');
	//Fixed Load
	Route::resource('fixed-loads', 'FixedLoadController');
	//Fixed Rate
	Route::resource('fixed-rates', 'FixedRateController');
	//Electricity Bills
	Route::resource('electricity-bills', 'ElectricityBillController');
	Route::get('/electricity-bills/account/{id}','ElectricityBillController@accountHistory');
	Route::get('/electricity-bills/receive-payment/{id}','ElectricityBillController@getReceive');
	Route::post('/electricity-bills/receive-payment/{id}','ElectricityBillController@postReceive');
	Route::get('/electricity-bills/print/{id}','ElectricityBillController@Print');
	//Maintenance Payment
	Route::resource('payments', 'PaymentController');
	//Ajax
	Route::get('/checkbox/floors','FloorController@floorsCheckbox');
	Route::get('/dropdown/floors','FloorController@floorsDropdown');
	Route::get('/dropdown/floors1','FloorController@floorsDropdown1');
	Route::get('/dropdown/flats','FlatController@flatsDropdown');
	Route::get('/dropdown/flats1','FlatController@flatsDropdown1');
	Route::get('/input/owner','OwnerController@ownerInput');
	Route::get('/input/owner2','OwnerController@ownerInput2');
	Route::get('/input/owner3','OwnerController@ownerInput3');
	Route::get('/calculate/advance','MaintenanceAdvanceController@calculate');
	Route::get('/theme/color','ThemeController@index');
	//Logout
	Route::get('/logout', function () {
	    Auth::logout();
	    return redirect('/');
	});
});
