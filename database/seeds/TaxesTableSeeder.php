<?php

use Illuminate\Database\Seeder;
use App\Tax;

class TaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tax::create([
            'name' => 'CGST',
            'rate' => '6',
            'is_active' => '1',
        ]);
        Tax::create([
            'name' => 'SGST',
            'rate' => '9',
            'is_active' => '1',
        ]);
    }
}
