<?php

use Illuminate\Database\Seeder;
use App\FixedRate;

class FixedRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FixedRate::create([
            'capacity_charge' => '75',
            'unit_charge' => '6',
            'applicable_from' => '2018-06-01',
        ]);
    }
}
