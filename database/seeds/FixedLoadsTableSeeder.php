<?php

use Illuminate\Database\Seeder;
use App\FixedLoad;

class FixedLoadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FixedLoad::create([
            'flat_type_id' => '1',
            'capacity' => '50',
        ]);
        FixedLoad::create([
            'flat_type_id' => '2',
            'capacity' => '100',
        ]);
        FixedLoad::create([
            'flat_type_id' => '3',
            'capacity' => '150',
        ]);
    }
}
