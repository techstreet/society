<?php

use Illuminate\Database\Seeder;
use App\MaintenanceRate;

class MaintenanceRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MaintenanceRate::create([
            'flat_type_id' => '1',
            'type' => '2',
            'rate' => '1.10',
            'amount' => '550',
            'applicable_from' => '2018-06-01',
        ]);
        MaintenanceRate::create([
            'flat_type_id' => '2',
            'type' => '2',
            'rate' => '1.20',
            'amount' => '1200',
            'applicable_from' => '2018-06-01',
        ]);
        MaintenanceRate::create([
            'flat_type_id' => '3',
            'type' => '1',
            'rate' => '500',
            'amount' => '500',
            'applicable_from' => '2018-06-01',
        ]);
    }
}
