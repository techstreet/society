<?php

use Illuminate\Database\Seeder;
use App\FlatType;

class FlatTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FlatType::create([
            'name' => '1BHK',
            'rooms' => '1',
            'wash_rooms' => '1',
            'store_rooms' => '0',
            'area' => '500',
        ]);
        FlatType::create([
            'name' => '2BHK',
            'rooms' => '2',
            'wash_rooms' => '1',
            'store_rooms' => '1',
            'area' => '1000',
        ]);
        FlatType::create([
            'name' => '3BHK',
            'rooms' => '3',
            'wash_rooms' => '2',
            'store_rooms' => '1',
            'area' => '1500',
        ]);
    }
}
