<?php

use Illuminate\Database\Seeder;
use App\Tower;

class TowersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tower::create([
            'name' => 'Tower A',
        ]);
        Tower::create([
            'name' => 'Tower B',
        ]);
        Tower::create([
            'name' => 'Tower C',
        ]);
        Tower::create([
            'name' => 'Tower D',
        ]);
    }
}
