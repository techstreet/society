<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(TowersTableSeeder::class);
        $this->call(FlatTypesTableSeeder::class);
        $this->call(TaxesTableSeeder::class);
        $this->call(MaintenanceRatesTableSeeder::class);
        $this->call(FixedLoadsTableSeeder::class);
        $this->call(FixedRatesTableSeeder::class);
    }
}
