<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('type')->nullable()->comment('1 - Society Owner, 2 - Flat Owner');
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->integer('mobile')->nullable();
			$table->integer('phone')->nullable();
			$table->string('gstin')->nullable();
			$table->text('address', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('groups');
	}

}
