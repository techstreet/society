<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaintenanceRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maintenance_rates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('flat_type_id')->nullable();
			$table->integer('type')->nullable()->comment('1 - Fixed, 2 - Per Square Feet');
			$table->decimal('rate', 15)->nullable();
			$table->decimal('amount', 15)->nullable();
			$table->date('applicable_from')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maintenance_rates');
	}

}
