<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRentersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('renters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tower_id')->nullable();
			$table->integer('floor_id')->nullable();
			$table->integer('flat_id')->nullable();
			$table->string('name')->nullable();
			$table->string('mobile', 10)->nullable();
			$table->string('email')->nullable();
			$table->text('address', 65535)->nullable();
			$table->decimal('dues', 15)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('renters');
	}

}
