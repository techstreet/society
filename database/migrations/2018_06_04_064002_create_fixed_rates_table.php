<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixedRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fixed_rates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('capacity_charge', 15)->nullable();
			$table->decimal('unit_charge', 15)->nullable();
			$table->date('applicable_from')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fixed_rates');
	}

}
