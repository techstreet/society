<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOwnersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('owners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tower_id')->nullable();
			$table->integer('floor_id')->nullable();
			$table->integer('flat_id')->nullable();
			$table->integer('flat_type_id')->nullable();
			$table->string('first_owner')->nullable();
			$table->string('second_owner')->nullable();
			$table->string('third_owner')->nullable();
			$table->string('mobile', 10)->nullable();
			$table->string('email')->nullable();
			$table->text('address', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('owners');
	}

}
