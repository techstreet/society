<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElectricityBillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('electricity_bills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('opening_reading_id')->nullable();
			$table->integer('tower_id')->nullable();
			$table->integer('floor_id')->nullable();
			$table->integer('flat_id')->nullable();
			$table->integer('opening_reading')->nullable();
			$table->integer('closing_reading')->nullable();
			$table->decimal('capacity', 15)->nullable();
			$table->decimal('capacity_charge', 15)->nullable();
			$table->decimal('unit_charge', 15)->nullable();
			$table->date('entry_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('electricity_bills');
	}

}
