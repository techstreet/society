<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOpeningBalancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('opening_balances', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tower_id')->nullable();
			$table->integer('floor_id')->nullable();
			$table->integer('flat_id')->nullable();
			$table->decimal('debit', 15);
			$table->text('comments', 65535)->nullable();
			$table->date('entry_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('opening_balances');
	}

}
