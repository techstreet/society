<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOpeningReadingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('opening_readings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tower_id')->nullable();
			$table->integer('floor_id')->nullable();
			$table->integer('flat_id')->nullable();
			$table->integer('reading')->nullable();
			$table->date('entry_date')->nullable();
			$table->text('comments', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('opening_readings');
	}

}
