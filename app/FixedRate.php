<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixedRate extends Model
{
    use SoftDeletes;
    public $fillable = ['capacity_charge','unit_charge','applicable_from'];
}
