<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parking extends Model
{
    use SoftDeletes;
    public $fillable = ['tower_id','floor_id','flat_id','parking_level','parking_zone','parking_number'];
}
