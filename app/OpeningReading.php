<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpeningReading extends Model
{
    use SoftDeletes;
    public $fillable = ['tower_id','floor_id','flat_id','reading','entry_date','comments'];
}
