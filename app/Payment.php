<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    protected $table = 'maintenance_dues';
    use SoftDeletes;
    public $fillable = ['flat_id','month','year','debit','credit','comments'];
}
