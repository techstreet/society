<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceDue extends Model
{
    use SoftDeletes;
    public $fillable = ['flat_id','entry_date','debit','credit','balance','comments'];
}
