<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use SoftDeletes;
    public $fillable = ['tower_id','floor_id','flat_id','flat_type_id','first_owner','second_owner','third_owner','mobile','email','address'];
}
