<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceRate extends Model
{
    use SoftDeletes;
    public $fillable = ['flat_type_id','type','rate','amount','applicable_from'];
}
