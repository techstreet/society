<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaintenanceAdvance extends Model
{
    use SoftDeletes;
    public $fillable = ['tower_id','floor_id','flat_id','amount','comments','month','year'];
}
