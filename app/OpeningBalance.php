<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpeningBalance extends Model
{
    use SoftDeletes;
    public $fillable = ['tower_id','floor_id','flat_id','debit','entry_date','comments'];
}
