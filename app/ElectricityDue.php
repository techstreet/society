<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ElectricityDue extends Model
{
    use SoftDeletes;
    public $fillable = ['electricity_bill_id','tower_id','floor_id','flat_id','entry_date','debit','credit','balance','comments'];
}
