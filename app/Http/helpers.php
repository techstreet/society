<?php
if (! function_exists('date_dfy')) {
    function date_dfy($date){
		if($date == '' || $date == '0000-00-00' || $date == '0000-00-00 00:00:00'){
			return '';
		}
		else{
			return date_format(date_create($date),"d-F-Y");
		}
	}
}
if (! function_exists('date_ymd')) {
    function date_ymd($date){
		if($date == '' || $date == '0000-00-00' || $date == '0000-00-00 00:00:00'){
			return '';
		}
		else{
			return date_format(date_create($date),"Y-m-d");
		}
	}
}
if (! function_exists('am_pm')) {
    function am_pm($time){
		if($time == ''){
			return '';
		}
		else{
			$new_time = new DateTime($time);
			return $new_time->format('h:i A');
		}
	}
}
if (! function_exists('getMonthName')) {
    function getMonthName($number){
		$months = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
		$date = getdate();
		return $months[$number];
	}
}
if (! function_exists('noneIfEmpty')) {
    function noneIfEmpty($data){
		if(empty($data)){
			return 'None';
		}
		else{
			return $data;
		}
	}
}
if (! function_exists('zeroIfEmpty')) {
    function zeroIfEmpty($data){
		if(empty($data)){
			return '0';
		}
		else{
			return $data;
		}
	}
}
if (! function_exists('defaultAvatar')) {
    function defaultAvatar($gender){
		if($gender == 'female'){
			return 'default_female.png';
		}
		else{
			return 'default_male.png';
		}
	}
}
if (! function_exists('themeColor')) {
    function themeColor(){
		$theme = DB::table('theme')
			->where([
			['user_id', Auth::id()],
			])
			->first();
		if(!empty($theme)){
			return $theme->color;
		}
		else{
			return 'green';
		}
	}
}
if (! function_exists('getBalance')) {
    function getBalance($flat_id)
	{
		$res = DB::table('maintenance_dues')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
			->where([
				['flat_id',$flat_id],
				['deleted_at',NULL],
				])
			->first();
		if($res){
			return $res->debit-$res->credit;
		}
		else{
			return 0;
		}
	}
}
if (! function_exists('duesGenerated')) {
    function duesGenerated($current_month)
	{
		$res = DB::table('maintenance_dues')
			->where([
				['deleted_at',NULL],
				])
			->whereMonth('entry_date',$current_month)
			->first();
		if($res){
			return true;
		}
		else{
			return false;
		}
	}
}
?>