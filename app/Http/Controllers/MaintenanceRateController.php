<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\MaintenanceRate;
use App\FlatType;
use DB;

class MaintenanceRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$maintenance_rates = MaintenanceRate::all();
        $maintenance_rates = DB::table('maintenance_rates')
			->select('maintenance_rates.*','flat_types.name as flat_type_name')
			->leftJoin('flat_types','flat_types.id','=','maintenance_rates.flat_type_id')
			->where([
			['maintenance_rates.deleted_at', NULL],
			])
            ->get();
        return view('maintenance_rates.index',compact('maintenance_rates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $flat_types = FlatType::all();
        return view('maintenance_rates.create',compact('flat_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'flat_type_id'=>'required',
            'type'=>'required',
            'rate'=>'required',
            'applicable_from'=>'required',
        ]);

        if($request->type == '2'){
            $flat_type = FlatType::find($request->flat_type_id);
            $area = $flat_type->area;
            $amount = $request->rate*$area;
        }
        else{
            $amount = $request->rate;
        }

        MaintenanceRate::create([
            'flat_type_id'=>$request->flat_type_id,
            'type'=>$request->type,
            'rate'=>$request->rate,
            'amount'=>$amount,
            'applicable_from'=> date_format(date_create($request->applicable_from),"Y-m-d")
        ]);
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$MaintenanceRate = MaintenanceRate::find($id);
        $MaintenanceRate = DB::table('maintenance_rates')
			->select('maintenance_rates.*','flat_types.name as flat_type_name')
			->leftJoin('flat_types','flat_types.id','=','maintenance_rates.flat_type_id')
			->where([
			['maintenance_rates.id', $id],
			])
            ->first();
        return view('maintenance_rates.show',compact('MaintenanceRate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $MaintenanceRate = MaintenanceRate::find($id);
        $flat_types = FlatType::all();
        return view('maintenance_rates.edit',compact('MaintenanceRate','flat_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'flat_type_id'=>'required',
            'type'=>'required',
            'rate'=>'required',
            'applicable_from'=>'required',
        ]);

        if($request->type == '2'){
            $flat_type = FlatType::find($request->flat_type_id);
            $area = $flat_type->area;
            $amount = $request->rate*$area;
        }
        else{
            $amount = $request->rate;
        }

        MaintenanceRate::find($id)->update([
            'flat_type_id'=>$request->flat_type_id,
            'type'=>$request->type,
            'rate'=>$request->rate,
            'amount'=>$amount,
            'applicable_from'=> date_format(date_create($request->applicable_from),"Y-m-d")
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $MaintenanceRate = MaintenanceRate::findOrFail($id);

        $MaintenanceRate->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
