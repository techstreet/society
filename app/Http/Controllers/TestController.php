<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Artisan;

class TestController extends Controller
{
    public function index()
    {
		Artisan::call('backup:run');
    }
}
