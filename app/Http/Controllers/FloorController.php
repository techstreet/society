<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Floor;
use App\Tower;
use DB;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$floors = Floor::groupBy('tower_id')->select('tower_id', DB::raw('count(*) as total'))->get();
        // echo "<pre>";
        // print_r($floors);
        // die;
        $floors = Floor::groupBy('tower_id')
			->select('floors.*','towers.name as tower_name', DB::raw('count(*) as total'))
			->leftJoin('towers','towers.id','=','floors.tower_id')
            ->get();
        return view('floors.index',compact('floors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = Tower::all();
        return view('floors.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_number'=>'required',
        ]);

        $floor_number = $request->floor_number;
        $tower_id = $request->tower_id;
        $count = count($floor_number);
		for($i=0; $i<$count; $i++){
            Floor::create([
                'tower_id'=>$tower_id,
                'floor_number'=>$floor_number[$i],
            ]);
		}
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $floors = DB::table('floors')
            ->select('floors.*','towers.name as tower_name')
            ->leftJoin('towers','towers.id','=','floors.tower_id')
			->where([
            ['floors.tower_id', $id],
            ['floors.deleted_at', NULL],
			])
			->orderBy('floors.id')
            ->get();
            
        return view('floors.show',compact('floors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $floors = DB::table('floors')
            ->select('floors.*','towers.name as tower_name')
            ->leftJoin('towers','towers.id','=','floors.tower_id')
			->where([
            ['floors.tower_id', $id],
            ['floors.deleted_at', NULL],
			])
			->orderBy('floors.floor_number')
            ->get();

        return view('floors.edit',compact('floors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'floor_number'=>'required',
        ]);

        $floor = Floor::where('tower_id',$id)->firstOrFail();
        $floor->where('tower_id',$id)->delete();

        $floor_number = $request->floor_number;
        $count = count($floor_number);
		for($i=0; $i<$count; $i++){
            Floor::create([
                'tower_id'=>$id,
                'floor_number'=>$floor_number[$i],
            ]);
		}
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $floor = Floor::where('tower_id',$id)->firstOrFail();
        $floor->where('tower_id',$id)->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }

    public function floorsCheckbox(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floors = DB::table('floors')
            ->select('floors.*','towers.name as tower_name')
            ->leftJoin('towers','towers.id','=','floors.tower_id')
			->where([
            ['floors.tower_id', $tower_id],
            ['floors.deleted_at', NULL],
			])
			->orderBy('floors.id')
            ->get();

        foreach ($floors as $value){ ?>
            <input class="floorChkbox" style="margin:5px; vertical-align:middle" type="checkbox" name="floor_number[]" value="<?php echo $value->id.':'.$value->floor_number; ?>" checked/><?php echo $value->floor_number; ?>
        <?php }
    }

    public function floorsDropdown1(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floors = DB::table('floors')
            ->select('floors.*','towers.name as tower_name')
            ->leftJoin('towers','towers.id','=','floors.tower_id')
			->where([
            ['floors.tower_id', $tower_id],
            ['floors.deleted_at', NULL],
			])
			->orderBy('floors.id')
            ->get();

        ?> <option value = "">Select</option> <?php
        foreach ($floors as $value){ ?>
            <option value="<?php echo $value->id ?>"><?php echo $value->floor_number ?></option>
        <?php }
    }

    public function floorsDropdown(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        ?> <option value = "">Select</option> <?php
        foreach ($floors as $value){ ?>
            <option value="<?php echo $value->floor_id ?>"><?php echo $value->floor_number ?></option>
        <?php }
    }
}
