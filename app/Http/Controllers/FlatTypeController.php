<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FlatType;

class FlatTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flat_types = FlatType::all();
        return view('flat_types.index',compact('flat_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('flat_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:255',
            'rooms'=>'required',
            'wash_rooms'=>'required',
            'store_rooms'=>'required',
            'area'=>'required',
        ]);

        FlatType::create($request->all());
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $FlatType = FlatType::find($id);
        return view('flat_types.show',compact('FlatType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $FlatType = FlatType::find($id);
        return view('flat_types.edit',compact('FlatType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|max:255',
            'rooms'=>'required',
            'wash_rooms'=>'required',
            'store_rooms'=>'required',
            'area'=>'required',
        ]);

        FlatType::find($id)->update($request->all());
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $FlatType = FlatType::findOrFail($id);

        $FlatType->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
