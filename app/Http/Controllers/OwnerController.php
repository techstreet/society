<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Owner;
use App\Tower;
use App\Floor;
use App\Flat;
use App\FlatType;
use DB;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = DB::table('owners')
            ->select('owners.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name','flat_types.name as flat_type')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
            ->leftJoin('flat_types','flat_types.id','=','owners.flat_type_id')
            ->whereNull('owners.deleted_at')
            ->get();
        return view('owners.index',compact('owners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers= Tower::all();
        $flat_types= FlatType::all();
        return view('owners.create',compact('towers','flat_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'flat_type_id'=>'required',
            'first_owner'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);

        Owner::create($request->all());
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$owner = Owner::find($id);
        $owner = DB::table('owners')
            ->select('owners.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name','flat_types.name as flat_type')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
            ->leftJoin('flat_types','flat_types.id','=','owners.flat_type_id')
            ->whereNull('owners.deleted_at')
            ->where('owners.id',$id)
            ->first();
        return view('owners.show',compact('owner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $owner = Owner::find($id);
        $floors = Floor::where('tower_id',$owner->tower_id)->get();
        $flats = Flat::where([
            ['tower_id',$owner->tower_id],
            ['floor_id',$owner->floor_id],
        ])->get();
        $towers= Tower::all();
        $flat_types= FlatType::all();
        return view('owners.edit',compact('owner','towers','flat_types','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'flat_type_id'=>'required',
            'first_owner'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);

        Owner::find($id)->update($request->all());
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owner = Owner::findOrFail($id);

        $owner->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }

    public function ownerInput(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floor_id = $request->input('floor_id');
        $flat_id = $request->input('flat_id');
        $floors = DB::table('owners')
			->where([
            ['tower_id', $tower_id],
            ['floor_id', $floor_id],
            ['flat_id', $flat_id],
            ['owners.deleted_at', NULL],
			])
            ->first();
        ?>
        <div class="form-group">
            <label for="owner">First Owner</label>
            <input type="text" class="form-control" value="<?php echo $floors->first_owner; ?>" disabled>
        </div>
        <?php
    }
    public function ownerInput2(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floor_id = $request->input('floor_id');
        $flat_id = $request->input('flat_id');
        $floors = DB::table('owners')
			->where([
            ['tower_id', $tower_id],
            ['floor_id', $floor_id],
            ['flat_id', $flat_id],
            ['owners.deleted_at', NULL],
			])
            ->first();

        $electricity_bills = DB::table('electricity_bills')
			->where([
            ['flat_id', $flat_id],
            ['deleted_at', NULL],
            ])
            ->orderBy('id','DESC')
            ->first();

        if($electricity_bills){
            $opening_reading = $electricity_bills->closing_reading;
        }
        else{
            $opening_reading = 0;
        }

        ?>
        <div class="form-group">
            <label for="owner">First Owner</label>
            <input type="text" class="form-control" value="<?php echo $floors->first_owner; ?>" disabled>
        </div>
        <?php
        ?>
        <div class="form-group">
            <label for="openingReading">Opening Reading</label>
            <input type="text" class="form-control" value="<?php echo $opening_reading; ?>" disabled>
        </div>
        <?php
    }
    public function ownerInput3(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floor_id = $request->input('floor_id');
        $flat_id = $request->input('flat_id');
        $floors = DB::table('owners')
			->where([
            ['tower_id', $tower_id],
            ['floor_id', $floor_id],
            ['flat_id', $flat_id],
            ['owners.deleted_at', NULL],
			])
            ->first();

        $maintenance_dues = DB::table('maintenance_dues')
			->where([
            ['flat_id', $flat_id],
            ['deleted_at', NULL],
            ])
            ->orderBy('id','DESC')
            ->first();

        if($maintenance_dues){
            $balance = $maintenance_dues->balance;
        }
        else{
            $balance = 0;
        }

        ?>
        <div class="form-group">
            <label for="owner">First Owner</label>
            <input type="text" class="form-control" value="<?php echo $floors->first_owner; ?>" disabled>
        </div>
        <?php
        ?>
        <div class="form-group">
            <label for="balance">Balance</label>
            <input type="text" class="form-control" value="<?php echo $balance; ?>" disabled>
        </div>
        <?php
    }
}
