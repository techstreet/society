<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpeningReading;
use App\ElectricityBill;
use App\Tower;
use App\Floor;
use App\Flat;
use App\Owner;
use DB;

class OpeningReadingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$opening_readings = OpeningReading::all();
        $opening_readings = DB::table('opening_readings')
            ->select('opening_readings.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','opening_readings.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('opening_readings.deleted_at')
            ->get();
        return view('opening_readings.index',compact('opening_readings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        return view('opening_readings.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'reading'=>'required',
            'entry_date'=>'required|date',
        ]);

        $fixed_rates = DB::table('fixed_rates')->where('deleted_at', NULL)->orderBy('applicable_from','DESC')->first();

        $capacity_charge = $fixed_rates->capacity_charge;
        $unit_charge = $fixed_rates->unit_charge;

        // Get Flat Type
        $owners = DB::table('owners')
            ->where([
                ['tower_id', $request->tower_id],
                ['floor_id', $request->floor_id],
                ['flat_id', $request->flat_id],
                ['deleted_at', NULL],
            ])
            ->first();

        $flat_type_id = $owners->flat_type_id;
        
        // Get Capacity
        $fixed_loads = DB::table('fixed_loads')
        ->where([
            ['flat_type_id', $flat_type_id],
            ['deleted_at', NULL],
        ])
        ->first();

        $capacity = $fixed_loads->capacity;

        $data = OpeningReading::create([
            'tower_id'=>$request->tower_id,
            'floor_id'=>$request->floor_id,
            'flat_id'=>$request->flat_id,
            'reading'=>$request->reading,
            'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
            'comments'=>$request->comments,
        ]);

        ElectricityBill::create([
            'opening_reading_id'=>$data->id,
            'tower_id'=>$request->tower_id,
            'floor_id'=>$request->floor_id,
            'flat_id'=>$request->flat_id,
            'opening_reading'=>$request->reading,
            'closing_reading'=>$request->reading,
            'capacity'=>$capacity,
            'capacity_charge'=>$capacity_charge,
            'unit_charge'=>$unit_charge,
            'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
        ]);
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$opening_reading = OpeningReading::find($id);
        $opening_reading = DB::table('opening_readings')
            ->select('opening_readings.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','opening_readings.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('opening_readings.deleted_at')
            ->where('opening_readings.id',$id)
            ->first();
        return view('opening_readings.show',compact('opening_reading'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opening_reading = OpeningReading::find($id);
        $owner = Owner::where('flat_id',$opening_reading->flat_id)->first();
        $tower_id = $owner->tower_id;
        $floor_id = $owner->floor_id;

        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();

        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.floor_id', $floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.flat_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();

        return view('opening_readings.edit',compact('opening_reading','towers','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'reading'=>'required',
            'entry_date'=>'required|date',
        ]);

        OpeningReading::find($id)->update([
            'tower_id'=>$request->tower_id,
            'floor_id'=>$request->floor_id,
            'flat_id'=>$request->flat_id,
            'reading'=>$request->reading,
            'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
            'comments'=>$request->comments,
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $opening_reading = OpeningReading::findOrFail($id);

        $opening_reading->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
