<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flat;
use App\Tower;
use App\MaintenanceDue;
use DB;

class FlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flats = Flat::groupBy('tower_id','floor_id')
			->select('flats.*','towers.name as tower_name','floors.floor_number as floor_name', DB::raw('count(*) as total'))
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->get();
        return view('flats.index',compact('flats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = Tower::all();
        return view('flats.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_number'=>'required',
            'number_flats'=>'required',
        ]);

        $tower_id = $request->tower_id;
        $floor_number = $request->floor_number;
        $number_flats = $request->number_flats;

        $count = count($floor_number);

		for($i=0; $i<$count; $i++){
            for($j=0; $j<$number_flats; $j++){
                
                $floor = explode(":",$floor_number[$i]);
                $floor_no = $floor[1];
                Flat::create([
                    'tower_id'=>$tower_id,
                    'floor_id'=>$floor_number[$i],
                    'flat_number'=>$floor_no.str_pad($j+1,2,'0',STR_PAD_LEFT),
                ]);
            }
        }
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $flat = Flat::findOrFail($id);
        $tower_id = $flat->tower_id;
        $floor_id = $flat->floor_id;

        $flats = $flat->select('flats.*','towers.name as tower_name','floors.floor_number as floor_name')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->orderBy('flats.flat_number')
            ->where([
                ['flats.tower_id',$tower_id],
                ['flats.floor_id',$floor_id],
            ])->get();

        return view('flats.show',compact('flats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $flat = Flat::findOrFail($id);
        $tower_id = $flat->tower_id;
        $floor_id = $flat->floor_id;

        $flats = $flat->select('flats.*','towers.name as tower_name','floors.floor_number as floor_name')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->orderBy('flats.flat_number')
            ->where([
                ['flats.tower_id',$tower_id],
                ['flats.floor_id',$floor_id],
            ])->get();
        
        return view('flats.edit',compact('flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'flat_number'=>'required',
        ]);

        $flat = Flat::findOrFail($id);
        $tower_id = $flat->tower_id;
        $floor_id = $flat->floor_id;

        $flat->where([
            ['tower_id',$tower_id],
            ['floor_id',$floor_id],
        ])
        ->delete();

        $flat_number = $request->flat_number;
        $count = count($flat_number);

		for($i=0; $i<$count; $i++){
            Flat::create([
                'tower_id'=>$tower_id,
                'floor_id'=>$floor_id,
                'flat_number'=>$flat_number[$i],
            ]);
		}
        
        return redirect()->route('flats.index')->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flat = Flat::findOrFail($id);
        $tower_id = $flat->tower_id;
        $floor_id = $flat->floor_id;

        $flat->where([
            ['tower_id',$tower_id],
            ['floor_id',$floor_id],
        ])->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }

    public function flatsDropdown1(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floor_id = $request->input('floor_id');
        $flats = Flat::where([
            ['tower_id', $tower_id],
            ['floor_id', $floor_id],
            ])
            ->orderBy('flat_number')
            ->get();

        ?> <option value = "">Select</option> <?php
        foreach ($flats as $value){ ?>
            <option value="<?php echo $value->id ?>"><?php echo $value->flat_number ?></option>
        <?php }
    }

    public function flatsDropdown(Request $request)
    {
        $tower_id = $request->input('tower_id');
        $floor_id = $request->input('floor_id');
        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.floor_id', $floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();

        ?> <option value = "">Select</option> <?php
        foreach ($flats as $value){ ?>
            <option value="<?php echo $value->flat_id ?>"><?php echo $value->flat_number ?></option>
        <?php }
    }
}
