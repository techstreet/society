<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\FixedLoad;
use App\FlatType;
use DB;

class FixedLoadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$fixed_loads = FixedLoad::all();
        $fixed_loads = DB::table('fixed_loads')
			->select('fixed_loads.*','flat_types.name as flat_type_name')
			->leftJoin('flat_types','flat_types.id','=','fixed_loads.flat_type_id')
			->where([
			['fixed_loads.deleted_at', NULL],
			])
            ->get();
        return view('fixed_loads.index',compact('fixed_loads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $flat_types = FlatType::all();
        return view('fixed_loads.create',compact('flat_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'flat_type_id'=>'required',
            'capacity'=>'required',
        ]);

        FixedLoad::create([
            'flat_type_id'=>$request->flat_type_id,
            'capacity'=>$request->capacity,
            'comments'=>$request->comments
        ]);
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$FixedLoad = FixedLoad::find($id);
        $FixedLoad = DB::table('fixed_loads')
			->select('fixed_loads.*','flat_types.name as flat_type_name')
			->leftJoin('flat_types','flat_types.id','=','fixed_loads.flat_type_id')
			->where([
			['fixed_loads.id', $id],
			])
            ->first();
        return view('fixed_loads.show',compact('FixedLoad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $FixedLoad = FixedLoad::find($id);
        $flat_types = FlatType::all();
        return view('fixed_loads.edit',compact('FixedLoad','flat_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'flat_type_id'=>'required',
            'capacity'=>'required',
        ]);

        FixedLoad::find($id)->update([
            'flat_type_id'=>$request->flat_type_id,
            'capacity'=>$request->capacity,
            'comments'=>$request->comments
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $FixedLoad = FixedLoad::findOrFail($id);

        $FixedLoad->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
