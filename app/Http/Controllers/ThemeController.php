<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Artisan;

class ThemeController extends Controller
{
    public function index(Request $request)
    {
      return DB::table('theme')
            ->where('user_id', Auth::id())
            ->update(['color' => $request->color]);
    }
}
