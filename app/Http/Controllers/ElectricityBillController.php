<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElectricityBill;
use App\ElectricityDue;
use App\Tower;
use App\Floor;
use App\Flat;
use App\Owner;
use DB;
use Carbon\Carbon;

class ElectricityBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $flat_id = $request->flat_id;
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
            ->where([
            ['owners.deleted_at', NULL],
            ])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        if(!empty($flat_id)){
            $electricity_bills = DB::table('electricity_dues')
            ->select(DB::raw('sum(electricity_dues.debit) as debit_total,sum(electricity_dues.credit) as credit_total'),'electricity_dues.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','electricity_dues.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->where('electricity_dues.flat_id',$flat_id)
            ->whereNull('electricity_dues.deleted_at')
            ->groupBy('flat_id')
            ->get();
        }
        else{
            $electricity_bills = DB::table('electricity_dues')
            ->select(DB::raw('sum(electricity_dues.debit) as debit_total,sum(electricity_dues.credit) as credit_total'),'electricity_dues.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','electricity_dues.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('electricity_dues.deleted_at')
            ->groupBy('flat_id')
            ->get();
        }
        
        $count = $electricity_bills->count();
        return view('electricity_bills.index',compact('electricity_bills','count','towers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        return view('electricity_bills.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'closing_reading'=>'required',
            'entry_date'=>'required|date',
        ]);

        DB::transaction(function () use($request)
        {
            // Get Opening & Closing Reading
            $data = ElectricityBill::where([
                ['tower_id', $request->tower_id],
                ['floor_id', $request->floor_id],
                ['flat_id', $request->flat_id],
            ])
            ->orderBy('id','DESC')
            ->first();

            $opening_reading = 0;

            if($data){
                $opening_reading = $data->closing_reading;
            }

            $closing_reading = $opening_reading+$request->closing_reading;

            // Get capacity_charge & unit_charge
            $fixed_rates = DB::table('fixed_rates')->where('deleted_at', NULL)->orderBy('applicable_from','DESC')->first();

            $capacity_charge = $fixed_rates->capacity_charge;
            $unit_charge = $fixed_rates->unit_charge;

            // Calculate Unit Difference
            $unit_diff = $closing_reading-$opening_reading;

            // Get Flat Type
            $owners = DB::table('owners')
                ->where([
                    ['tower_id', $request->tower_id],
                    ['floor_id', $request->floor_id],
                    ['flat_id', $request->flat_id],
                    ['deleted_at', NULL],
                ])
                ->first();

            $flat_type_id = $owners->flat_type_id;
            
            // Get Capacity
            $fixed_loads = DB::table('fixed_loads')
            ->where([
                ['flat_type_id', $flat_type_id],
                ['deleted_at', NULL],
            ])
            ->first();

            $capacity = $fixed_loads->capacity;

            // Calculate Debit
            $debit = ($capacity*$capacity_charge)+($unit_diff*$unit_charge);

            $data1 = ElectricityBill::create([
                'tower_id'=>$request->tower_id,
                'floor_id'=>$request->floor_id,
                'flat_id'=>$request->flat_id,
                'opening_reading'=>$opening_reading,
                'closing_reading'=>$closing_reading,
                'capacity'=>$capacity,
                'capacity_charge'=>$capacity_charge,
                'unit_charge'=>$unit_charge,
                'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
            ]);

            $data2 = ElectricityDue::create([
                'electricity_bill_id'=>$data1->id,
                'tower_id'=>$request->tower_id,
                'floor_id'=>$request->floor_id,
                'flat_id'=>$request->flat_id,
                'debit'=>$debit,
                'comments'=>'Dues initiated',
                'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
            ]);

            $res = DB::table('electricity_dues')
            ->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            ->where([
                ['flat_id',$request->flat_id],
                ['deleted_at',NULL],
                ])
            ->first();

            DB::table('electricity_dues')
            ->where('id', $data2->id)
            ->update(['balance' => $res->debit - $res->credit]);
        });
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$electricity_bill = ElectricityBill::find($id);
        $electricity_bill = DB::table('electricity_bills')
            ->select('electricity_bills.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','electricity_bills.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('electricity_bills.deleted_at')
            ->where('electricity_bills.id',$id)
            ->first();
        return view('electricity_bills.show',compact('electricity_bill'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $electricity_bill = ElectricityBill::find($id);
        $owner = Owner::where('flat_id',$electricity_bill->flat_id)->first();
        $tower_id = $owner->tower_id;
        $floor_id = $owner->floor_id;

        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();

        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.floor_id', $floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.flat_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();

        return view('electricity_bills.edit',compact('electricity_bill','towers','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort(404);
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'closing_reading'=>'required',
            'entry_date'=>'required|date',
        ]);

        ElectricityBill::find($id)->update([
            'tower_id'=>$request->tower_id,
            'floor_id'=>$request->floor_id,
            'flat_id'=>$request->flat_id,
            'closing_reading'=>$request->closing_reading,
            'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        $electricity_bill = ElectricityBill::findOrFail($id);

        $electricity_bill->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }

    public function accountHistory($flat_id)
    {
        $flats = DB::table('electricity_dues')
			->where([
            ['flat_id', $flat_id],  
			['deleted_at', NULL],
			])
			->orderBy('id')
            ->get();

        return view('electricity_bills.account',compact('flats','flat_id'));
    }

    public function getReceive($flat_id)
    {
        return view('electricity_bills.receive',compact('flat_id'));
    }

    public function postReceive(Request $request, $flat_id)
    {
        $this->validate($request,[
            'credit'=>'required',
            'entry_date'=>'required|date',
            'comments'=>'required',
        ]);

        DB::transaction(function () use($request, $flat_id)
		{
            $data = ElectricityDue::create([
                'flat_id'=>$flat_id,
                'entry_date'=>date_ymd($request->entry_date),
                'credit'=>$request->credit,
                'comments'=>$request->comments,
            ]);
    
            $res = DB::table('electricity_dues')
            ->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            ->where([
                ['flat_id',$flat_id],
                ['deleted_at',NULL],
                ])
            ->first();
    
            DB::table('electricity_dues')
            ->where('id', $data->id)
            ->update(['balance' => $res->debit - $res->credit]);

        });

        return redirect()->back()->with('success', 'Payment successfully received!');
    }
    public function Print($flat_id)
    {
        $owner = DB::table('owners')
        ->whereNull('deleted_at')
        ->where('flat_id',$flat_id)
        ->first();
        $electricity_bill = DB::table('electricity_bills')
        ->whereNull('deleted_at')
        ->where('flat_id',$flat_id)
        ->first();
        return view('electricity_bills.print',compact('owner','electricity_bill'));
    }
}
