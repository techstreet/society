<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FixedRate;

class FixedRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fixed_rates = FixedRate::all();
        return view('fixed_rates.index',compact('fixed_rates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fixed_rates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'capacity_charge'=>'required',
            'unit_charge'=>'required',
            'applicable_from'=>'required|date',
        ]);

        FixedRate::create([
            'capacity_charge'=>$request->capacity_charge,
            'unit_charge'=>$request->unit_charge,
            'applicable_from'=> date_format(date_create($request->applicable_from),"Y-m-d")
        ]);
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $FixedRate = FixedRate::find($id);
        return view('fixed_rates.show',compact('FixedRate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $FixedRate = FixedRate::find($id);
        return view('fixed_rates.edit',compact('FixedRate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'capacity_charge'=>'required',
            'unit_charge'=>'required',
            'applicable_from'=>'required|date',
        ]);

        FixedRate::find($id)->update([
            'capacity_charge'=>$request->capacity_charge,
            'unit_charge'=>$request->unit_charge,
            'applicable_from'=> date_format(date_create($request->applicable_from),"Y-m-d")
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $FixedRate = FixedRate::findOrFail($id);

        $FixedRate->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
