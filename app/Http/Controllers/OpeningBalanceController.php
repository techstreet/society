<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpeningBalance;
use App\Tower;
use App\Floor;
use App\Flat;
use App\Owner;
use DB;

class OpeningBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$opening_balances = OpeningBalance::all();
        $opening_balances = DB::table('opening_balances')
            ->select('opening_balances.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','opening_balances.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('opening_balances.deleted_at')
            ->get();
        return view('opening_balances.index',compact('opening_balances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        return view('opening_balances.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'debit'=>'required',
            'entry_date'=>'required|date',
        ]);

        OpeningBalance::create([
            'tower_id'=>$request->tower_id,
            'floor_id'=>$request->floor_id,
            'flat_id'=>$request->flat_id,
            'debit'=>$request->debit,
            'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
            'comments'=>$request->comments,
        ]);
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$opening_balance = OpeningBalance::find($id);
        $opening_balance = DB::table('opening_balances')
            ->select('opening_balances.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','opening_balances.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('opening_balances.deleted_at')
            ->where('opening_balances.id',$id)
            ->first();
        return view('opening_balances.show',compact('opening_balance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opening_balance = OpeningBalance::find($id);
        $owner = Owner::where('flat_id',$opening_balance->flat_id)->first();
        $tower_id = $owner->tower_id;
        $floor_id = $owner->floor_id;

        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();

        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $tower_id],
            ['owners.floor_id', $floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.flat_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();

        return view('opening_balances.edit',compact('opening_balance','towers','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'debit'=>'required',
            'entry_date'=>'required|date',
        ]);

        OpeningBalance::find($id)->update([
            'tower_id'=>$request->tower_id,
            'floor_id'=>$request->floor_id,
            'flat_id'=>$request->flat_id,
            'debit'=>$request->debit,
            'entry_date'=> date_format(date_create($request->entry_date),"Y-m-d"),
            'comments'=>$request->comments,
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $opening_balance = OpeningBalance::findOrFail($id);

        $opening_balance->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
