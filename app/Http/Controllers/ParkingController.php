<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;
use App\Tower;
use App\Floor;
use App\Flat;
use DB;

class ParkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$parkings = Parking::all();
        $parkings = DB::table('parkings')
            ->select('parkings.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('towers','towers.id','=','parkings.tower_id')
            ->leftJoin('floors','floors.id','=','parkings.floor_id')
            ->leftJoin('flats','flats.id','=','parkings.flat_id')
            ->whereNull('parkings.deleted_at')
            ->get();
        return view('parkings.index',compact('parkings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        return view('parkings.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'parking_level'=>'required',
            'parking_zone'=>'required',
            'parking_number'=>'required',
        ]);

        Parking::create($request->all());
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$parking = Parking::find($id);
        $parking = DB::table('parkings')
            ->select('parkings.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('towers','towers.id','=','parkings.tower_id')
            ->leftJoin('floors','floors.id','=','parkings.floor_id')
            ->leftJoin('flats','flats.id','=','parkings.flat_id')
            ->whereNull('parkings.deleted_at')
            ->where('parkings.id',$id)
            ->first();
        return view('parkings.show',compact('parking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parking = Parking::find($id);
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();

        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $parking->tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $parking->tower_id],
            ['owners.floor_id', $parking->floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.flat_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();
        return view('parkings.edit',compact('parking','towers','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'parking_level'=>'required',
            'parking_zone'=>'required',
            'parking_number'=>'required',
        ]);

        Parking::find($id)->update($request->all());
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parking = Parking::findOrFail($id);

        $parking->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
