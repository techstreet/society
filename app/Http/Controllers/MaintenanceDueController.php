<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaintenanceDue;
use App\MaintenanceRate;
use App\FlatType;
use App\Owner;
use App\Tower;
use DB;
use Carbon\Carbon;

class MaintenanceDueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $flat_id = $request->flat_id;
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        if(!empty($flat_id)){
            $maintenance_dues = DB::table('maintenance_dues')
            ->select(DB::raw('sum(maintenance_dues.debit) as debit_total,sum(maintenance_dues.credit) as credit_total'),'maintenance_dues.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','maintenance_dues.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->where('maintenance_dues.flat_id',$flat_id)
            ->whereNull('maintenance_dues.deleted_at')
            ->groupBy('flat_id')
            ->get();
        }
        else{
            $maintenance_dues = DB::table('maintenance_dues')
            ->select(DB::raw('sum(maintenance_dues.debit) as debit_total,sum(maintenance_dues.credit) as credit_total'),'maintenance_dues.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('flats','flats.id','=','maintenance_dues.flat_id')
            ->leftJoin('towers','towers.id','=','flats.tower_id')
            ->leftJoin('floors','floors.id','=','flats.floor_id')
            ->whereNull('maintenance_dues.deleted_at')
            ->groupBy('flat_id')
            ->get();
        }
        $count = $maintenance_dues->count();
        $now = Carbon::now();
        $current_month = $now->month;
        return view('maintenance_dues.index',compact('maintenance_dues','count','towers','current_month'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $flat_types = FlatType::all();
        return view('maintenance_dues.create',compact('flat_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use($request)
		{
            $now = Carbon::now();
            $today = Carbon::today();
            $current_year = $now->year;
            $current_month = $now->month;

            $owners= Owner::all();

            foreach($owners as $value){
                $maintenance_rate = MaintenanceRate::where([
                    ['flat_type_id',$value->flat_type_id],
                    ['applicable_from', '<=', $today],
                ])->orderBy('applicable_from','desc')->first();
                $amount = $maintenance_rate->amount;
                $applicable_from = $maintenance_rate->applicable_from;

                if(Carbon::parse($applicable_from)->lte(Carbon::today())){
                    $data = MaintenanceDue::create([
                        'flat_id'=>$value->flat_id,
                        'debit'=>$amount,
                        'entry_date'=>$today,
                        'comments'=>'Dues initiated',
                    ]);

                    $res = DB::table('maintenance_dues')
                    ->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
                    ->where([
                        ['flat_id',$value->flat_id],
                        ['deleted_at',NULL],
                        ])
                    ->first();

                    DB::table('maintenance_dues')
                    ->where('id', $data->id)
                    ->update(['balance' => $res->debit - $res->credit]);

                }
            }
        });

        return redirect()->back()->with('success', 'Record successfully generated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$MaintenanceDue = MaintenanceDue::find($id);
        $MaintenanceDue = DB::table('maintenance_dues')
			->select('maintenance_dues.*','flat_types.name as flat_type_name')
			->leftJoin('flat_types','flat_types.id','=','maintenance_dues.flat_type_id')
			->where([
			['maintenance_dues.id', $id],
			])
            ->first();
        return view('maintenance_dues.show',compact('MaintenanceDue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $MaintenanceDue = MaintenanceDue::find($id);
        $flat_types = FlatType::all();
        return view('maintenance_dues.edit',compact('MaintenanceDue','flat_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'flat_type_id'=>'required',
            'type'=>'required',
            'rate'=>'required',
            'applicable_from'=>'required',
        ]);

        if($request->type == '2'){
            $flat_type = FlatType::find($request->flat_type_id);
            $area = $flat_type->area;
            $amount = $request->rate*$area;
        }
        else{
            $amount = $request->rate;
        }

        MaintenanceDue::find($id)->update([
            'flat_type_id'=>$request->flat_type_id,
            'type'=>$request->type,
            'rate'=>$request->rate,
            'amount'=>$amount,
            'applicable_from'=> date_format(date_create($request->applicable_from),"Y-m-d")
        ]);
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $MaintenanceDue = MaintenanceDue::findOrFail($id);

        $MaintenanceDue->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }

    public function accountHistory($flat_id)
    {
        $flats = DB::table('maintenance_dues')
			->where([
            ['flat_id', $flat_id],  
			['deleted_at', NULL],
			])
			->orderBy('id')
            ->get();

        return view('maintenance_dues.account',compact('flats','flat_id'));
    }

    public function getReceive($flat_id)
    {
        return view('maintenance_dues.receive',compact('flat_id'));
    }

    public function postReceive(Request $request, $flat_id)
    {
        $this->validate($request,[
            'credit'=>'required',
            'entry_date'=>'required|date',
            'comments'=>'required',
        ]);

        DB::transaction(function () use($request, $flat_id)
		{
            $data = MaintenanceDue::create([
                'flat_id'=>$flat_id,
                'entry_date'=>date_ymd($request->entry_date),
                'credit'=>$request->credit,
                'comments'=>$request->comments,
            ]);
    
            $res = DB::table('maintenance_dues')
            ->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            ->where([
                ['flat_id',$flat_id],
                ['deleted_at',NULL],
                ])
            ->first();
    
            DB::table('maintenance_dues')
            ->where('id', $data->id)
            ->update(['balance' => $res->debit - $res->credit]);

        });

        return redirect()->back()->with('success', 'Payment successfully received!');
    }
}
