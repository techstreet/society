<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaintenanceAdvance;
use App\MaintenanceRate;
use App\MaintenanceDue;
use App\Tower;
use App\Floor;
use App\Flat;
use DB;
use Carbon\Carbon;

class MaintenanceAdvanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$maintenance_advances = MaintenanceAdvance::all();
        $maintenance_advances = DB::table('maintenance_advances')
            ->select('maintenance_advances.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('towers','towers.id','=','maintenance_advances.tower_id')
            ->leftJoin('floors','floors.id','=','maintenance_advances.floor_id')
            ->leftJoin('flats','flats.id','=','maintenance_advances.flat_id')
            ->whereNull('maintenance_advances.deleted_at')
            ->get();
        return view('maintenance_advances.index',compact('maintenance_advances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        return view('maintenance_advances.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'month'=>'required',
            'year'=>'required',
        ]);

        DB::transaction(function () use($request)
        {
            $now = Carbon::now();

            $month1 = $now->month;
            $year1 = $now->year;

            $month2 = $request->month;
            $year2 = $request->year;

            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            // Get Flat Type
            $owners = DB::table('owners')
                ->where([
                    ['tower_id', $request->tower_id],
                    ['floor_id', $request->floor_id],
                    ['flat_id', $request->flat_id],
                    ['deleted_at', NULL],
                ])
                ->first();

            $flat_type_id = $owners->flat_type_id;

            $maintenance_rate = MaintenanceRate::where([
                ['flat_type_id',$flat_type_id],
                ['applicable_from', '<=', Carbon::today()],
            ])->orderBy('applicable_from','desc')->first();
            $amount = $maintenance_rate->amount*$diff;
            $applicable_from = $maintenance_rate->applicable_from;

            if(Carbon::parse($applicable_from)->lte(Carbon::today())){
                
                MaintenanceAdvance::create([
                    'tower_id'=>$request->tower_id,
                    'floor_id'=>$request->floor_id,
                    'flat_id'=>$request->flat_id,
                    'amount'=>$amount,
                    'comments'=>$request->comments,
                    'month'=>$request->month,
                    'year'=>$request->year,
                ]);

                $data = MaintenanceDue::create([
                    'flat_id'=>$request->flat_id,
                    'debit'=>$amount,
                    'comments'=>$request->comments,
                    'entry_date'=>$now,
                ]);

                $res = DB::table('maintenance_dues')
                ->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
                ->where([
                    ['flat_id',$request->flat_id],
                    ['deleted_at',NULL],
                    ])
                ->first();
        
                DB::table('maintenance_dues')
                ->where('id', $data->id)
                ->update(['balance' => $res->debit - $res->credit]);

            }
        });
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$maintenance_advance = MaintenanceAdvance::find($id);
        $maintenance_advance = DB::table('maintenance_advances')
            ->select('maintenance_advances.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('towers','towers.id','=','maintenance_advances.tower_id')
            ->leftJoin('floors','floors.id','=','maintenance_advances.floor_id')
            ->leftJoin('flats','flats.id','=','maintenance_advances.flat_id')
            ->whereNull('maintenance_advances.deleted_at')
            ->where('maintenance_advances.id',$id)
            ->first();
        return view('maintenance_advances.show',compact('maintenance_advance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maintenance_advance = MaintenanceAdvance::find($id);
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();

        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $maintenance_advance->tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $maintenance_advance->tower_id],
            ['owners.floor_id', $maintenance_advance->floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.flat_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();
        return view('maintenance_advances.edit',compact('maintenance_advance','towers','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'name'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);

        MaintenanceAdvance::find($id)->update($request->all());
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maintenance_advance = MaintenanceAdvance::findOrFail($id);

        $maintenance_advance->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
    public function calculate(Request $request)
    {
        $now = Carbon::now();

        $month1 = $now->month;
        $year1 = $now->year;

        $month2 = $request->month;
        $year2 = $request->year;

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

        // Get Flat Type
        $owners = DB::table('owners')
            ->where([
                ['flat_id', $request->flat_id],
                ['deleted_at', NULL],
            ])
            ->first();

        $flat_type_id = $owners->flat_type_id;

        $maintenance_rate = MaintenanceRate::where([
            ['flat_type_id',$flat_type_id],
            ['applicable_from', '<=', Carbon::today()],
        ])->orderBy('applicable_from','desc')->first();
        $amount = $maintenance_rate->amount*$diff;
        $applicable_from = $maintenance_rate->applicable_from;

        if(Carbon::parse($applicable_from)->lte(Carbon::today())){
            $amount = $maintenance_rate->amount*$diff;
        }
        else{
            $amount = 0;
        }
        ?>
        <div class="form-group">
            <label for="amount">Amount</label>
            <input type="text" class="form-control" value="<?php echo $amount; ?>" disabled>
        </div>
        <?php
    }
}
