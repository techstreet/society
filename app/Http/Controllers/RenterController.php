<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Renter;
use App\Tower;
use App\Floor;
use App\Flat;
use DB;

class RenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$renters = Renter::all();
        $renters = DB::table('renters')
            ->select('renters.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('towers','towers.id','=','renters.tower_id')
            ->leftJoin('floors','floors.id','=','renters.floor_id')
            ->leftJoin('flats','flats.id','=','renters.flat_id')
            ->whereNull('renters.deleted_at')
            ->get();
        return view('renters.index',compact('renters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();
        return view('renters.create',compact('towers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'name'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);

        Renter::create($request->all());
        
        return redirect()->back()->with('success', 'Record successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$renter = Renter::find($id);
        $renter = DB::table('renters')
            ->select('renters.*','towers.name as tower_name','floors.floor_number as floor_name','flats.flat_number as flat_name')
            ->leftJoin('towers','towers.id','=','renters.tower_id')
            ->leftJoin('floors','floors.id','=','renters.floor_id')
            ->leftJoin('flats','flats.id','=','renters.flat_id')
            ->whereNull('renters.deleted_at')
            ->where('renters.id',$id)
            ->first();
        return view('renters.show',compact('renter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $renter = Renter::find($id);
        $towers = DB::table('owners')
            ->select('owners.*','towers.name as name')
            ->leftJoin('towers','towers.id','=','owners.tower_id')
			->where([
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.tower_id')
            ->groupBy('owners.tower_id')
            ->get();

        $floors = DB::table('owners')
            ->select('owners.*','floors.floor_number as floor_number')
            ->leftJoin('floors','floors.id','=','owners.floor_id')
			->where([
            ['owners.tower_id', $renter->tower_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.floor_id')
            ->groupBy('owners.tower_id','owners.floor_id')
            ->get();

        $flats = DB::table('owners')
            ->select('owners.*','flats.flat_number as flat_number')
            ->leftJoin('flats','flats.id','=','owners.flat_id')
			->where([
            ['owners.tower_id', $renter->tower_id],
            ['owners.floor_id', $renter->floor_id],
            ['owners.deleted_at', NULL],
			])
            ->orderBy('owners.flat_id')
            ->groupBy('owners.tower_id','owners.floor_id','owners.flat_id')
            ->get();
        return view('renters.edit',compact('renter','towers','floors','flats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tower_id'=>'required',
            'floor_id'=>'required',
            'flat_id'=>'required',
            'name'=>'required',
            'mobile'=>'required',
            'email'=>'required',
            'address'=>'required',
        ]);

        Renter::find($id)->update($request->all());
        
        return redirect()->back()->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $renter = Renter::findOrFail($id);

        $renter->delete();

        return redirect()->back()->with('success', 'Record successfully deleted!');
    }
}
