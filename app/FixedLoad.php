<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixedLoad extends Model
{
    use SoftDeletes;
    public $fillable = ['flat_type_id','capacity','comments'];
}
