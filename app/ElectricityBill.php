<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ElectricityBill extends Model
{
    use SoftDeletes;
    public $fillable = ['opening_reading_id','tower_id','floor_id','flat_id','opening_reading','closing_reading','capacity','capacity_charge','unit_charge','entry_date'];
}
